﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Blob.Startup))]
namespace Blob
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
