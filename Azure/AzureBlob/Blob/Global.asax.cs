﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Blob;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;


namespace Blob
{
    public partial class BlobForm : System.Web.UI.Page
    {
        /// <summary>
        /// Container exists or not and gallery refresh blobs display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    this.EnsureContainerExists();
                }
                this.RefreshGallery();
            }
            catch (System.Net.WebException we)
            {
                status.Text = "Network error: " + we.Message;
                if (we.Status == System.Net.WebExceptionStatus.ConnectFailure)
                {
                    status.Text += "<br />Please check if the blob service is running at " +
                    ConfigurationManager.AppSettings["storageEndpoint"];
                }
            }
            catch (StorageException se)
            {
                status.Text = "Storage service error: " + se.Message;
            }
        }

        /// <summary>
        /// upload blob and refresh galary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void upload_Click(object sender, EventArgs e)
        {
            try
            {
                if (imageFile.HasFile)
                {
                    status.Text = "Inserted [" + imageFile.FileName + "] - Content Type [" + imageFile.PostedFile.ContentType + "] - Length [" + imageFile.PostedFile.ContentLength + "]";

                    this.SaveImage(
                      Guid.NewGuid().ToString(),
                      imageName.Text,
                      imageDescription.Text,
                      imageTags.Text,
                      imageFile.FileName,
                      imageFile.PostedFile.ContentType,
                      imageFile.FileBytes
                    );

                    RefreshGallery();
                }
                else
                {
                    status.Text = "No image file";
                }
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }

        }

        /// <summary>
        /// Cast out blob instance and bind it's metadata to metadata repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnBlobDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var metadataRepeater = e.Item.FindControl("blobMetadata") as Repeater;
                var blob = ((ListViewDataItem)(e.Item)).DataItem as CloudBlob;

                // If this blob is a snapshot, rename button to "Delete Snapshot"
                if (blob != null)
                {
                    if (blob.SnapshotTime.HasValue)
                    {
                        var delBtn = e.Item.FindControl("deleteBlob") as LinkButton;

                        if (delBtn != null)
                        {
                            delBtn.Text = "Delete Snapshot";
                            var snapshotRequest = BlobRequest.Get(new Uri(delBtn.CommandArgument), 0, blob.SnapshotTime.Value, null);
                            delBtn.CommandArgument = snapshotRequest.RequestUri.AbsoluteUri;
                        }

                        var snapshotBtn = e.Item.FindControl("SnapshotBlob") as LinkButton;
                        if (snapshotBtn != null) snapshotBtn.Visible = false;
                    }

                    if (metadataRepeater != null)
                    {
                        //bind to metadata
                        metadataRepeater.DataSource = from key in blob.Metadata.AllKeys
                                                      select new
                                                      {
                                                          Name = key,
                                                          Value = blob.Metadata[key]
                                                      };
                        metadataRepeater.DataBind();
                    }
                }
            }
        }

        /// <summary>
        /// Delete an image blob by Uri
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDeleteImage(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    var blobUri = (string)e.CommandArgument;
                    var blob = this.GetContainer().GetBlobReference(blobUri);
                    blob.DeleteIfExists();
                    status.Text = "Blob has been deleted successfully.";
                }
            }
            catch (StorageClientException se)
            {
                status.Text = "Storage client error: " + se.Message;
            }
            catch (Exception ex) { status.Text = ex.Message; }

            RefreshGallery();
        }

        /// <summary>
        /// copy blobs and update image name and ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnCopyImage(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Copy")
                {
                    // Prepare an Id for the copied blob
                    var newId = Guid.NewGuid();

                    // Get source blob
                    var blobUri = (string)e.CommandArgument;
                    var srcBlob = this.GetContainer().GetBlobReference(blobUri);

                    // Create new blob
                    var newBlob = this.GetContainer().GetBlobReference(newId.ToString());

                    // Copy content from source blob
                    newBlob.CopyFromBlob(srcBlob);

                    // Explicitly get metadata for new blob
                    newBlob.FetchAttributes(new BlobRequestOptions { BlobListingDetails = BlobListingDetails.Metadata });

                    // Change metadata on the new blob to reflect this is a copy via UI
                    newBlob.Metadata["ImageName"] = "Copy of \"" + newBlob.Metadata["ImageName"] + "\"";
                    newBlob.Metadata["Id"] = newId.ToString();
                    newBlob.SetMetadata();
                    status.Text = "Blob has been copied successfully.";
                    // Render all blobs
                    RefreshGallery();
                }
            }
            catch (Exception ex) { status.Text = ex.Message; }
        }

        /// <summary>
        /// Blob Snapshot 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSnapshotImage(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Snapshot")
                {
                    // Get source blob
                    var blobUri = (string)e.CommandArgument;
                    var srcBlob = this.GetContainer().GetBlobReference(blobUri);

                    // Create a snapshot
                    var snapshot = srcBlob.CreateSnapshot();

                    status.Text = "A snapshot has been taken for image blob:" + srcBlob.Uri + " at " + snapshot.SnapshotTime;

                    RefreshGallery();
                }
            }
            catch (Exception ex) { status.Text = ex.Message; }
        }

        /// <summary>
        /// Check container exists or not and also assign permission
        /// </summary>
        private void EnsureContainerExists()
        {
            var container = GetContainer();
            container.CreateIfNotExist();

            var permissions = container.GetPermissions();
            permissions.PublicAccess = BlobContainerPublicAccessType.Container;
            container.SetPermissions(permissions);
        }

        /// <summary>
        /// Get DataConnectionString and ContainerName
        /// </summary>
        /// <returns></returns>
        private CloudBlobContainer GetContainer()
        {
            // Get a handle on account, create a blob service client and get container proxy
            var account = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            var client = account.CreateCloudBlobClient();

            return client.GetContainerReference(RoleEnvironment.GetConfigurationSettingValue("ContainerName"));
        }

        /// <summary>
        /// Refesh a galary get all the blobs
        /// </summary>
        private void RefreshGallery()
        {

            images.DataSource =
              this.GetContainer().ListBlobs(new BlobRequestOptions()
              {
                  Timeout = TimeSpan.FromSeconds(10.0), // time stamp 10 second 
                  UseFlatBlobListing = true,
                  BlobListingDetails = BlobListingDetails.All
              });
            images.DataBind();
        }

        /// <summary>
        /// Save a blob
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="tags"></param>
        /// <param name="fileName"></param>
        /// <param name="contentType"></param>
        /// <param name="data"></param>
        private void SaveImage(string id, string name, string description, string tags, string fileName, string contentType, byte[] data)
        {
            // Create a blob in container and upload image bytes to it
            var blob = this.GetContainer().GetBlobReference(name);

            blob.Properties.ContentType = contentType;

            // Create some metadata for this image
            var metadata = new NameValueCollection();
            metadata["Id"] = id;
            metadata["Filename"] = fileName;
            metadata["ImageName"] = String.IsNullOrEmpty(name) ? "unknown" : name;
            metadata["Description"] = String.IsNullOrEmpty(description) ? "unknown" : description;
            metadata["Tags"] = String.IsNullOrEmpty(tags) ? "unknown" : tags;

            // Add and commit metadata to blob
            blob.Metadata.Add(metadata);
            blob.UploadByteArray(data);
        }

    }
}
