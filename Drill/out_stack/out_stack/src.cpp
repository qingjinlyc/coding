#include <iostream>
#include <queue>
#include <stack>
#include <vector>
using namespace ::std;
void print_seq(queue<int>  que, stack<int> s, int num){
	while (!que.empty()){
		int v = que.front();
		que.pop();
		cout << v << "\t";
	}
	cout << num << "\t";
	while (!s.empty()){
		int v = s.top();
		s.pop();
		cout << v << "\t";
	}
	cout << endl;
}

void add_stack(vector<int> nums, queue<int> q, stack<int> s, int count){
	if (count == nums.size() - 1)print_seq(q, s, nums[count]);
	else{
		queue<int> temp = q;
		q.push(nums[count]);
		count++;
		add_stack(nums, q, s, count);
		count--;
		q = temp;
		s.push(nums[count]);
		count++;
		add_stack(nums, q, s, count);
		count--;
	}
}

int main(){
	vector<int> nums;
	stack<int> s;
	queue<int> q;
	int N, n, count = 0;
	cin >> N;
	while (N){
		cin >> n;
		nums.push_back(n);
		N--;
	}
	add_stack(nums, q, s, count);
	return 0;
}
