#include <stdio.h>
#include <stdlib.h>

struct single
{
	int coe;
	int exp;
};

struct single* data;
int len=0;
int count=0;

void expand()
{
	int i;
	struct single* newdata;

	newdata=(struct single*)malloc((len+100)*sizeof(struct single));
	for(i=0;i<len;i++)
		newdata[i]=data[i];
	data=newdata;
	len+=100;
	return ;
}

int main(void)
{
	char tmp;
	int thisnum=0;
	int isneg=0;
	int iscoe=1;
	int index=0;

	int i;

	expand();
	
	while((tmp=getchar())!='\n')
	{
		if(tmp=='-')
		{
			isneg=1;
			continue;
		}
		if(tmp==' ')
		{
			if(isneg==1)
				thisnum=-thisnum;
			if(iscoe==1)
			{
				count++;
				if(count>len)
					expand();
				data[index].coe=thisnum;
				thisnum=0;
				iscoe=0;
				isneg=0;
			}
			else
			{	
				data[index].exp=thisnum;
				thisnum=0;
				iscoe=1;
				isneg=0;
				index++;
			}
		}
		else
			thisnum=thisnum*10+(tmp-'0');
	}
	if(isneg==1)
		thisnum=-thisnum;
	if(iscoe==1)
	{
		count++;
		if(count>len)
			expand();
		data[index].coe=thisnum;
		thisnum=0;
		iscoe=0;
		isneg=0;
	}
	else
	{	
		data[index].exp=thisnum;
		thisnum=0;
		iscoe=1;			
		isneg=0;
		index++;
	}



	if(data[0].exp!=0)
	{
		data[0].coe*=data[0].exp--;
		printf("%d %d",data[0].coe,data[0].exp);
	}

	for(i=1;i<count;i++)
	{
		if(data[i].exp!=0)
		{
			data[i].coe*=data[i].exp--;
			printf(" %d %d",data[i].coe,data[i].exp);
		}
	}

	return 0;
}