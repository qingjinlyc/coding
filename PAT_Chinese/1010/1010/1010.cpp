#include <stdio.h>

int main(){
	int a, n, i = 0;
	while(scanf("%d", &a) != EOF){
		scanf("%d", &n);
		if(n == 0){
			if(i == 0){
				printf("%d %d", n, n);
			}
			break;
		}
		if(i == 0){
			printf("%d ", a * n);
			i++;
		}
		else
			printf(" %d ", a * n);
		printf("%d", n - 1);
	}
	return 0;
}