#include <iostream>
#include <vector>
using namespace ::std;
class Outer{
public: char m_Out;
public:
	
	void print(){
		cout << "father:" << m_Out;
	}
	virtual void func(){ cout << "a"; };
	Outer() : m_Out('B'){ func(); };
	friend int operator + (Outer a, Outer b);
	static void f();
	static int a;
	inline void addnumber(int a){
		numbers.push_back(a);
	}
	inline void printnumber(){
		for (int i = 0; i < numbers.size(); ++i)cout << numbers[i] << "\t";
		cout << endl;
	}
	static void(*fp)();
	Outer & operator = (Outer & o) {
		m_Out = o.m_Out;
		return *this;
	}
	int assign;
protected:
	class Inner{
	public:
		static int m_Int;
		void Display();
	};
	void foo(){
		cout << "a\n";
	}
	vector<int> numbers;
public:
	static Inner* getInner(){
		return new Inner();
	}
};


class MyCString {
private :
	char * buf;
public:
	MyCString(){ buf = new char[10]; }
	~MyCString() { if (buf)delete buf; }
	void setstr(char * s);
	void printstr();
};
