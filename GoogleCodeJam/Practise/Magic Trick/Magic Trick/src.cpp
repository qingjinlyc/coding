#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <stdio.h>
using namespace ::std;

int findsame(int *, int *);
int main() {
	int first[4], second[4];
	int buf[4];
	int T, line, cur = 1;
	FILE *p = fopen("d:\\A-small-practice.in", "r");
	FILE *o = fopen("d:\\A-small-practice.out", "w");
	fscanf(p, "%d", &T);
	while (T--) {
		fscanf(p, "%d", &line);
		for (int i = 0; i < 4; ++i) {
			if (i + 1 == line) {
				fscanf(p, "%d %d %d %d", &first[0], &first[1], &first[2], &first[3]);
			}
			else
				fscanf(p, "%d %d %d %d", &buf[0], &buf[1], &buf[2], &buf[3]);
		}
		fscanf(p, "%d", &line);
		for (int i = 0; i < 4; ++i) {
			if (i + 1 == line) {
				fscanf(p, "%d %d %d %d", &second[0], &second[1], &second[2], &second[3]);
			}
			else
				fscanf(p, "%d %d %d %d", &buf[0], &buf[1], &buf[2], &buf[3]);
		}
		int ret = findsame(first, second);
		switch (ret)
		{
		case 0:fprintf(o, "Case #%d: %s\n", cur, "Volunteer cheated!");
			break;
		case -1:fprintf(o, "Case #%d: %s\n", cur, "Bad magician!");
			break;
		default:
			fprintf(o, "Case #%d: %d\n", cur, ret);
			break;
		}
		++cur;
	}
	fclose(p);
	fclose(o);
	return 0;
}

int findsame(int *f, int *l) {
	int place[17];
	memset(place, 0, 16 * sizeof(int));
	int count = 0, ret;
	for (int i = 0; i < 4; ++i) {
		place[f[i]] = 1;
	}
	for (int i = 0; i < 4; ++i) {
		if (place[l[i]] == 1){
			++count;
			ret = l[i];
		}
	}
	if (count == 1)return ret;
	if (count == 0)return 0;
	if (count > 1)return -1;
}