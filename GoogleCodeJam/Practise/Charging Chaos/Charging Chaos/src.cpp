#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <hash_set>
#include <stdio.h>
#include <queue>
#include <vector>

using namespace :: std;

void flip(vector<string> &str, int pos);
bool ok(vector<string> &ini, vector<string> &want);
int main() {
	int T, N, L;
	FILE *iF = fopen("A-large-practice.in", "r");
	FILE *oF = fopen("A-large-practice.out", "w");
	fscanf(iF, "%d", &T);
	char str[41];
	int cur = 0, last = 0, level = 0;
	for (int j = 1; j <= T; ++j) {
		vector<string> iniV, wantV;
		queue<vector<string> > q;
		queue<int> q2;
		bool found = false;
		level = 0;
		cur = 0;
		last = 0;
		fscanf(iF, "%d %d", &N, &L);
		string *tp = NULL;
		for (int l = 0; l < N; ++l){
			fscanf(iF, "%s", str);
			iniV.push_back(*(tp = new string(str)));
			delete tp;
		}
		for (int l = 0; l < N; ++l){
			fscanf(iF, "%s", str);
			wantV.push_back(*(tp = new string(str)));
			delete tp;
		}
		q.push(iniV);
		q2.push(0);
		last = 1;
		while (!q.empty() && !found) {
			vector<string> v = q.front();
			int start = q2.front();
			q.pop();
			q2.pop();
			--last;
			if (ok(v, wantV)) {
				fprintf(oF, "Case #%d: %d\n", j, level);
				found = true;
			}
			if (last == 0)++level;
			for (int k = start; k < L && !found; ++k) {
				vector<string> temp = v;
				flip(v, k);
				q.push(v);
				q2.push(k + 1);
				++cur;
				v = temp;
			}
			if (last == 0) {
				last = cur;
				cur = 0;
			}
		}
		if (!found)fprintf(oF, "Case #%d: NOT POSSIBLE\n", j);
	}
	fclose(iF);
	fclose(oF);
	return 0;
}

void flip(vector<string> &str, int pos){
	for (int i = 0; i < str.size(); ++i) {
		str[i][pos] = (str[i][pos] == '0' ? '1' : '0');
	}
}

bool ok(vector<string> &ini, vector<string> &want){
	hash_set<string> hash;
	for (int i = 0; i < ini.size(); ++i)hash.insert(ini[i]);
	for (int i = 0; i < want.size(); ++i) {
		if (hash.find(want[i]) == hash.end())return false;
	}
	return true;
}