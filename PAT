#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct{
	char ID[7];
	int C;
	int M;
	int E;
	int A;
	int oA;
	int oC;
	int oM;
	int oE;
}Student;

int * max(Student stu);
const char tag[4] = {'A', 'C', 'M', 'E'};
int compareC(const void * a, const void * b);
int compareM(const void * a, const void * b);
int compareE(const void * a, const void * b);
int compareA(const void * a, const void * b);
int main(){
	int N, M, i, *res, j, c;
	char input[7];
	char query[2000][7];
	Student * stu = NULL;
	scanf("%d %d", &N, &M);
	stu = (Student*)malloc(sizeof(Student) * N);
	for(i = 0; i < N; i++){
		scanf("%s %d %d %d", stu[i].ID, &stu[i].C, &stu[i].M, &stu[i].E);
		stu[i].A = (stu[i].C + stu[i].M + stu[i].E);
	}
	qsort(stu, N, sizeof(Student), compareC);
	j = stu[0].C;
	c = 1;
	for(i = 0; i < N; i++){
		if(i == 0)stu[i].oC = 1;
		else if(stu[i].C == stu[i - 1].C)stu[i].oC = c;
		else{
			stu[i].oC = i + 1;
			c = i + 1;
		}
	}
	qsort(stu, N, sizeof(Student), compareM);
	j = stu[0].M;
	c = 1;
	for(i = 0; i < N; i++){
		if(i == 0)stu[i].oM = 1;
		else if(stu[i].M == stu[i - 1].M)stu[i].oM = c;
		else{
			stu[i].oM = i + 1;
			c = i + 1;
		}
	}
	qsort(stu, N, sizeof(Student), compareE);
	j = stu[0].E;
	c = 1;
	for(i = 0; i < N; i++){
		if(i == 0)stu[i].oE = 1;
		else if(stu[i].E == stu[i - 1].E)stu[i].oE = c;
		else{
			stu[i].oE = i + 1;
			c = i + 1;
		}
	}
	qsort(stu, N, sizeof(Student), compareA);
	j = stu[0].A;
	c = 1;
	for(i = 0; i < N; i++){
		if(i == 0)stu[i].oA = 1;
		else if(stu[i].A == stu[i - 1].A)stu[i].oA = c;
		else{
			stu[i].oA = i + 1;
			c = i + 1;
		}
	}
	
	for(i = 0; i < M; i++){
		scanf("%s", &query[i]);
	}
	for(i = 0; i < M; i++){
		for(j = 0; j < N && strcmp(stu[j].ID, input); j++);
		if(j == N){
			printf("N/A\n");
			continue;
		}
		res = max(stu[j]);
		printf("%d %c\n", res[1], tag[res[0]]);
	}
	return 0;
}

int compareC(const void * a, const void * b){
	return (((Student *)a)->C - ((Student *)b)->C) < 0 ? 1: -1; 
}
int compareM(const void * a, const void * b){
	return (((Student *)a)->M - ((Student *)b)->M) < 0 ? 1: -1; 
}
int compareE(const void * a, const void * b){
	return (((Student *)a)->E - ((Student *)b)->E) < 0 ? 1: -1; 
}
int compareA(const void * a, const void * b){
	return (((Student *)a)->A - ((Student *)b)->A) < 0 ? 1: -1; 
}

int * max(Student stu){
	int res[2] = {0, stu.oA}, min = stu.oA;
	if(stu.oC < min){
		min = stu.oC;
		res[0] = 1;
		res[1] = min;
	}
	if(stu.oM < min){
		min = stu.oM;
		res[0] = 2;
		res[1] = min;
	}
	if(stu.oE < min){
		min = stu.oE;
		res[0] = 3;
		res[1] = min;
	}
	return (int *)res;
}