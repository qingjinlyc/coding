#define _CRT_SECURE_NO_WARNINGS
#include <unordered_map>
#include <stack>
#include <string>
#include <stdio.h>

using namespace::std;

int main(){
	unordered_map<string, pair<int, string>> nodes;
	stack<string> s;
	char p[6], next[6];
	int N, K;
	scanf("%s %d %d", p, &N, &K);
	string head(p);
	int val;
	for (int i = 0; i < N; i++){
		scanf("%s %d %s", p, &val, next);
		string ps(p), ns(next);
		nodes.insert(pair<string, pair<int, string>>(ps, pair<int, string>(val, ns)));
	}
	string work = head;
	unordered_map<string, pair<int, string>>::iterator it, itw;
	string last_tail = "";
	int first = 1;
	while (work != "-1"){
		itw = nodes.find(work);
		string last_next = itw->second.second;
		if (s.size() == K - 1){
			string now = work;
			if (first){
				head = work;
				first = 0;
			}
			while (!s.empty()){
				it = nodes.find(now);
				it->second.second = s.top();
				now = s.top();
				s.pop();
			}
			it = nodes.find(now);
			it->second.second = last_next;
			if (last_tail != ""){
				it = nodes.find(last_tail);
				it->second.second = work;
			}
			last_tail = now;
		}
		else{
			s.push(work);
		}
		work = last_next;
	}
	work = head;
	while (work != "-1"){
		it = nodes.find(work);
		printf("%s %d %s\n", it->first.c_str(), it->second.first, it->second.second.c_str());
		work = it->second.second;
	}
	return 0;
}