#include <stdio.h>

void to13(int ten, char * res);

int main(){
	int a, b, c;
	char resa[3], resb[3], resc[3];
	scanf("%d %d %d", &a, &b, &c);
	to13(a, (char *)resa);
	to13(b, (char *)resb);
	to13(c, (char *)resc);
	printf("#%s%s%s", resa, resb, resc);
	return 0;
}

void to13(int ten, char * res){
	res[1] = ten % 13;
	if(res[1] >= 10)res[1] = 'A' + res[1] - 10;
	else
		res[1] = '0' + res[1];
	ten /= 13;
	res[0] = ten % 13;
	if(res[0] >= 10)res[0] = 'A' + res[0] - 10;
	else
		res[0] = '0' + res[0];
	res[2] = 0;
}