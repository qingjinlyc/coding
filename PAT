import java.io.*;
import java.util.*;


public class Main {
	public static void main(String args[]){
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String line = null;
		int N;
		LinkedList<Integer> que = new LinkedList<Integer>();
		try {
			line = br.readLine();
			N = Integer.parseInt(line);
			int post[] = new int[N], mid[] = new int[N];
			line = br.readLine();
			String tempStr [] = line.split(" ");
			for(int i = 0; i < N; i++)post[i] = Integer.parseInt(tempStr[i]);
			line = br.readLine();
			tempStr = line.split(" ");
			for(int i = 0; i < N; i++)mid[i] = Integer.parseInt(tempStr[i]);
			
			System.out.print(post[post.length - 1]);
			int center = Main.findEle(mid, post[post.length - 1], 0, mid.length - 1);
			if(center != 0){
				que.addFirst(0);
				que.addFirst(center - 1);
				que.addFirst(0);
				que.addFirst(center - 1);
			}
			if(center != (mid.length - 1)){
				que.addFirst(center + 1);
				que.addFirst(mid.length - 1);
			}
			if((center - 1) != (post.length - 2)){
				que.addFirst(center);
				que.addFirst(mid.length - 2);
			}
			
			
			while(!que.isEmpty()){
				int from_mid = que.pollLast();
				int to_mid = que.pollLast();
				int from_post = 0;
				int to_post = 0;
				
				from_post = que.pollLast();
				to_post = que.pollLast();
				int root = getRoot(post, from_post, to_post);
				System.out.print(" " + root);
				center = Main.findEle(mid, root, from_mid, to_mid);
				if(center != from_mid){
					que.addFirst(from_mid);
					que.addFirst(center - 1);
					que.addFirst(from_post);
					que.addFirst(from_post + center - from_mid - 1);
				}
				if(center != to_mid){
					que.addFirst(center + 1);
					que.addFirst(to_mid);
				}
				if((from_post + center - from_mid - 1) != (to_post - 1)){
					que.addFirst(from_post + center - from_mid);
					que.addFirst(to_post - 1);
				}
			}
			
			System.out.print("\n");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static int getRoot(int post[], int from, int to){
		return post[to];
	}
	
	static int findEle(int array[], int ele, int from, int to){
		int i = from;
		for(; i <= to && array[i] != ele; i++);
		return i;
	}
}
