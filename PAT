#include <stdio.h>

#define UP 6
#define DOWN 4
#define STOP 5

int main(){
	int time = 0, N, i, floor, last_floor = 0;
	scanf("%d", &N);
	for(i = 0; i < N; i++){
		scanf("%d", &floor);
		time += ((floor - last_floor > 0)? (floor - last_floor) * UP: (last_floor - floor) * DOWN);
		time += 5;
		last_floor = floor;
	}
	printf("%d", time);
	return 0;
}