#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>

using namespace ::std;
void adjust(vector<int> & s, int start){
	int flag = 0;
	for (int i = 0; i < 2; ++i){
		for (int j = start; j < start + 2 - i; ++j){
			if (s[i] > s[j]){
				int temp = s[i];
				s[i] = s[j];
				s[j] = s[i];
				flag = 1;
			}
		}
		if (!flag)break;
	}
}
int main(){
	int T, N, x;
	scanf("%d", &T);
	while (T){
		vector<int> scores;
		scanf("%d", &N);
		for (int i = 0; i < N; ++i){
			scanf("%d", &x);
			scores.push_back(i);
		}

		adjust(scores, 0);
		for (int i = 3; i < N; i += 3){
			adjust(scores, i);
			for (int j = i; j < i + 3 && j < N; ++j){
				scores[2 - (j - i)] += scores[j];
			}
			adjust(scores, 0);
		}
		--T;
		printf("%d\n", scores[2] - scores[0]);
	}
	return 0;
}