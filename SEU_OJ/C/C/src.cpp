#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <vector>
#include <set>
using namespace ::std;
int d(vector<int> & route, long long int sum, int y, int * op, set<long long> * al);
int c(vector<int> & route, long long int sum, int y, int * op, set<long long> * al);
int b(vector<int> & route, long long int sum, int y, int * op, set<long long> * al);
int a(vector<int> & route, long long int sum, int y, int * op, set<long long> * al);


int main(){
	int T, op[4], x, y;
	scanf("%d", &T);
	while (T){
		scanf("%d %d %d %d %d %d", &op[0], &op[1], &op[2], &op[3], &x, &y);
		int found = 0;
		vector<int> route;
		while (!found){
			set<long long> al[4];
			found = found || a(route, x, y, op, al);
			if (!found)found = found || b(route, x, y, op, al);
		}
		printf("%d", x);
		for (int i = 0; i < route.size(); ++i){
			switch (route[i]){
			case 0:
				printf("+%d", op[0]);
				break;
			case 1:
				printf("/%d", op[1]);
				break;
			case 2:
				printf("*%d", op[2]);
				break;
			case 3:
				printf("-%d", op[3]);
				break;
			}
		}
		printf("\n");
		--T;
	}
	return 0;
}

int a(vector<int> & route, long long int sum, int y, int * op, set<long long> * al){
	int o = op[0], found = 0, res = o + sum;
	if (route.size() >= 32)return 0;
	set<long long> ::iterator it = al[0].find(sum);
	if (it != al[0].end())return 0;
	al[0].insert(sum);
	if (o + sum >= (long long)pow(2.0, 60.0))return 0;
	else{
		route.push_back(0);
		found = found || b(route, sum + o, y, op, al);
		if (!found)found = found || c(route, sum + o, y, op, al);
		if (!found)found = found || d(route, sum + o, y, op, al);
		if (!found)route.pop_back();
	}
	return found;
}
int b(vector<int> & route, long long int sum, int y, int * op, set<long long> * al){
	int o = op[1], res = sum / o, found = 0;
	if (sum < 0)return 0;
	if (route.size() >= 32)return 0;
	if (sum < y / 2)return 0;
	set<long long> ::iterator it = al[1].find(sum);
	if (it != al[1].end())return 0;
	al[1].insert(sum);
	if (res >= (long long)pow(2.0, 60.0))return 0;
	else{
		route.push_back(1);
		if(res > y / 2)found = found || a(route, res, y, op, al);
		if (!found)found = found || c(route, res, y, op, al);
		if (!found)found = found || d(route, res, y, op, al);
		if (!found)route.pop_back();
	}
	return found;
}
int c(vector<int> & route, long long int sum, int y, int * op, set<long long> * al){
	if (sum < 0)return 0;
	if (sum > y)return 0;
	int o = op[2], res = o * sum, found = 0;
	if (res == y){
		route.push_back(2);
		return 1;
	}
	if (route.size() >= 32)return 0;
	set<long long> ::iterator it = al[2].find(sum);
	if (it != al[2].end())return 0;
	al[2].insert(sum);
	if (res >= (long long)pow(2.0, 60.0))return 0;
	else{
		route.push_back(2);
		if(res < y)found = found || a(route, res, y, op, al);
		if (!found)found = found || b(route, res, y, op, al);
		if (!found && (res < 2 * y))found = found || d(route, res, y, op, al);
		if (!found)route.pop_back();
	}
	return found;
}
int d(vector<int> & route, long long int sum, int y, int * op, set<long long> * al){
	if (sum < 0)return 0;
	if (sum < y / 2)return 0;
	int o = op[3], res = sum - o, found = 0;
	if (res == y){
		route.push_back(3);
		return 1;
	}
	if (route.size() >= 32)return 0;
	set<long long> ::iterator it = al[3].find(sum);
	if (it != al[3].end())return 0;
	al[3].insert(sum);
	if (res >= (long long)pow(2.0, 60.0))return 0;
	if (res == y)return 1;
	else{
		route.push_back(3);
		found = found || a(route, res, y, op, al);
		if (!found)found = found || b(route, res, y, op, al);
		if (!found)found = found || d(route, res, y, op, al);
		if (!found)route.pop_back();
	}
	return found;
}