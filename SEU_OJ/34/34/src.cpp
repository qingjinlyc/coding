#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define MAX 100000

using namespace ::std;

int main(){
	int n, q, d, l, r, x, y, count = 0;
	while (scanf("%d %d", &n, &q) != 0){
		int a[MAX];
		for (int i = 0; i < n; ++i){
			scanf("%d", &d);
			a[i] = d;
		}
		for (int i = 0; i < q; ++i){
			scanf("%d %d %d %d", &l, &r, &x, &y);
			count = 0;
			for (int j = l; j <= r; ++j){
				if (a[j - 1] >= x && a[j - 1] <= y)++count;
			}
			printf("%d\n", count);
		}
	}
	return 0;
}