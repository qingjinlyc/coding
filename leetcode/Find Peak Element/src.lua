function findPeakElement(nums)
	return find(nums, 1, #nums)
end

function find(nums, s, e)
	if e == s then
		return nums[s]
	end
	st = s en = e mid = (s + e) / 2
	if nums[mid] > nums[mid - 1] and nums[mid] < nums[mid + 1] then
		return nums[mid]
	end

	if nums[mid] > nums[mid - 1] and nums[mid + 1] > nums[mid] then
		return find(nums, mid + 1, e)
	else
		return find(nums, s, mid - 1)
	end
end


nums = {2, 1, 3, 4, 0}

print (findPeakElement(nums))

