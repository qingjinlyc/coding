#include <string>
#include <vector>
using namespace :: std;
class Solution {
public:
    string getPermutation(int n, int k) {
        vector<int> already(n + 1, 0);
        int all = 0;
        int round = 0;
        string res = "";
        for(int i = 0; i < n; ++i) {
            all = N(n - 1 - i);
            round = k / all;
            int tn = 1;
            for(tn = 1; tn <= round + 1; ++tn){
                if(already[tn] == 1)++tn;
            }
			--tn;
			already[tn] = 1;
            res += (tn + '0');
            k = k - round * all;
        }
        return res;
    }
private:
    int N(int n) {
        int ret = 1;
        while(n>1){
            ret *= n;
            --n;
        }
        return ret;
    }
};

int main () {
	Solution s;
	string res = s.getPermutation(4, 8);
	return 0;
}