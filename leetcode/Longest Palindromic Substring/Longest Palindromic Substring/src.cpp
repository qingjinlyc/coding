#include <string>
#include <vector>

using namespace :: std;

class Solution {
public:
    string longestPalindrome(string s) {
        string ts;
        for(int i = 0, l = s.size(); i < 2 * l; s.insert(i, "#"), i = i + 2);
        s += "#";
        vector<int> P(s.size(), 0);
        int C = 1, R = 2;
        P[0] = 1;
        P[1] = 2;
        int ind = C + 1;
        while(ind < P.size()) {
            int l = C * 2 - ind;
            int up = R - ind + 1;
            if(ind >= R){
                P[ind] = 1;
                int rr = ind + 1, ll = 2 * ind - rr;
                while(ll > -1 && rr < P.size() && s[rr] == s[ll]) {
                    ++rr;
                    --ll;
                    ++P[ind];
                }
            }
            else if(P[l] < up)P[ind] = P[l];
            else {
                P[ind] = up;
                int k = R + 1, ll = 2 * ind - k;
                while(k < s.size() && ll > -1 && s[ll] == s[k]){
                    --ll;
                    ++k;
                    ++P[ind];
                }
            }
            if(P[ind] >= P[C]) {
                C = ind;
                R = ind + P[ind] - 1;
            }
            ++ind;
        }
        int max = P[1], p = 1;
        for(int i = 1; i < P.size(); ++i)
            if(P[i] > max){
                max = P[i];
                p = i;
            }
        for(int i = p - max + 1; i < p + (max - 1); ++i) {
            if(s[i] != '#')ts += s[i];
        }
        return ts;
    }
};

int main(){
	string str = "krivbgbgingspwekawchjdcpylvefubulvxneuizglrjkiaibrepifxpxfkczwoumkk";
	string ret = Solution().longestPalindrome(str);
	return 0;
}