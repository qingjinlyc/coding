#include <iostream>
using namespace::std;
struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
	ListNode *insertionSortList(ListNode *head) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		ListNode * work = head, *res = NULL, *ind = NULL, *last = NULL;
		int value;
		if(head == NULL)return NULL;
		res = new ListNode(head->val);
		head = head->next;
		//free(work);
		work = head;
		while(work){
			value = work->val;
			ind = res;
			last = NULL;
			for(; ind != NULL && value > ind->val; last = ind, ind = ind->next);
			if(last == NULL){
				last = new ListNode(value);
				last->next = res;
				res = last;
			}
			else{
				last->next = new ListNode(value);
				last->next->next = ind;
			}
			head = head->next;
			//free(work);
			work = head;
		}
		return res;
	}
};

int main(){
	ListNode head(3);
	head.next = new ListNode(4);
	head.next->next = new ListNode(1);
	Solution s;
	ListNode * res = s.insertionSortList(&head);
	return 0;
}