#include <string>

using namespace :: std;

class Solution {
public:
    string convertToTitle(int n) {
        string ret = "";
        while(true){
			if(n <= 26){
				if(n == 26)ret.insert(0, "Z");
				else{
					char temp = (n + 'A' - 1);
					char t[2] = {temp, 0};
					ret.insert(0, t);
				}
				break;
			}
            int l = n % 26;
            if(l == 0){
                ret.insert(0, "Z");
                n /= 26;
				--n;
            }
            else{
                char temp = (l + 'A' - 1);
				char t[2] = {temp, 0};
                ret.insert(0, t);
                n /= 26;
            }
        }
		return ret;
    }
};

int main () {
	string ret = Solution().convertToTitle(52);
	return 0;
}