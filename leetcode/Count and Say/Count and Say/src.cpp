#include <iostream>
#include <string>
using namespace ::std;

class Solution {
public:
	string countAndSay(int n) {
		string ret = "", base = "1";
		for (int i = 2; i <= n; ++i){
			ret = count(base);
			base = ret;
		}
		return ret;
	}
private:
	string count(string str){
		string count = "";
		for (int i = 0; i <= str.size(); ++i){
			if (str[i] == '1'){
				if (i < str.length() - 1 && str[i + 1] == '1'){
					count += "21";
					++i;
					continue;
				}
				else{
					count += "11";
				}
			}
			if (str[i] == '2'){
				count += "12";
			}
		}
		return count;
	}
};
