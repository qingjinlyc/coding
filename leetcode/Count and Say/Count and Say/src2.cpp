#include <iostream>
#include <string>
using namespace ::std;
class Solution {
public:
	string countAndSay(int n) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.

		string prev = "1";
		string cur = "";
		for (int i = 2; i <= n; ++i)
		{
			int p = 1;
			int len = prev.length();
			char c = prev[0];
			int count = 1;
			while (p<len)
			{
				if (c == prev[p])
				{
					++p; ++count;
				}
				else{
					cur += '0' + count;
					cur += c;
					count = 1;
					c = prev[p++];
				}
			}
			cur += '0' + count;
			cur += c;
			prev = cur;
			cur = "";
		}
		return prev;
	}
};
int main(){
	Solution s;
	string str = s.countAndSay(10);
	return 0;
}