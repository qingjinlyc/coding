#include <string>
#include <unordered_map>
using namespace::std;

class Solution {
public:
	int numDistinct(string S, string T) {
		if (S == T || T == "")return 1;
		if (S == "")return 0;
		int sum = 0;
		count(S, T, 0, 0, sum);
		return sum;
	}
	void count(string S, string T, int posS, int posT, int & sum) {
		if (posS == S.length() || S.length() - posS < T.length() - posT)return;
		int cur_sum = 0;
		for (int i = posS; i < S.length(); i++)
			if (S[i] == T[posT]) {
				if (posT != T.length() - 1)count(S, T, i + 1, posT + 1, sum);
				cur_sum++;
			}
		if (posT == T.length() - 1) {
			sum += cur_sum;
			return;
		}
	}
};

int main(){
	string S = "aabdbaabeeadcbbdedacbbeecbabebaeeecaeabaedadcbdbcdaabebdadbbaeabdadeaabbabbecebbebcaddaacccebeaeedababedeacdeaaaeeaecbe", T = "bddabdcae";
	Solution s;
	int res = s.numDistinct(S, T);
	return 0;
}