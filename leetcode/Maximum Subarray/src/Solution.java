
public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
		System.out.println(new Solution().maxSubArray(A));
		return;
	}
	public int maxSubArray(int[] A) {
		if(A.length == 0)return 0;
        int max = A[0];
        int sum = 0;
        for(int i = 0; i < A.length; ++i) {
        	sum += A[i];
        	if(sum > max)max = sum;
        	if(sum < 0)sum = 0;
        }
        return max;
    }
}
