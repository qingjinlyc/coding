#include <string>

using namespace::std;

class Solution {
public:
	bool isPalindrome(string s) {
		if (s == "")return false;
		int len = s.length();
		int i = 0, j = len - 1;
		while(i <= j) {
			if (isvalid(s[i]) && isvalid(s[j])){
				if (equal(s[i], s[j])){
					i++;
					j--;
					continue;
				}
				else return false;
			}
			if (!isvalid(s[i])){
				i++;
				continue;
			}
			if (!isvalid(s[j])){
				j--;
				continue;
			}
		}
		return true;
	}

	bool equal(char a, char b){
		return (a == b || a - b == 'A' - 'a' || a - b == 'a' - 'A');
	}

	bool isvalid(char a){
		return (a <= 'Z' && a >= 'A' || a <= 'z' && a >= 'a' || a <= '9' && a >= '0');
	}
};

int main(){
	string a = "l&*^*&^*    yc%^&^  &*s&(*&(*  c*)(*()*y *(&*^*&l";
	Solution s;
	bool res = s.isPalindrome(a);
	return 0;
}

