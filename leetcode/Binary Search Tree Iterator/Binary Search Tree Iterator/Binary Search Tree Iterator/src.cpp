#include <stack>
#include <iostream>
using namespace ::std;
/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class BSTIterator {
private:
	stack<TreeNode *> s;
public:
	BSTIterator(TreeNode *root) {
		while (!s.empty())s.pop();
		TreeNode *w = root;
		while (1) {
			if (!w)break;
			s.push(w);
			w = w->left;
		}
	}

	/** @return whether we have a next smallest number */
	bool hasNext() {
		return !s.empty();
	}

	/** @return the next smallest number */
	int next() {
		TreeNode *w = s.top();
		s.pop();
		int ret = w->val;
		if (w->right){
			w = w->right;
			while (1) {
				if (!w)break;
				s.push(w);
				w = w->left;
			}
		}
		return ret;
	}
};

int main(){
	TreeNode *root = new TreeNode(1);
	root->right = new TreeNode(2);
	BSTIterator i = BSTIterator(root);
	while (i.hasNext()) 
		cout << i.next();
	return 0;
}