#include <unordered_set>
#include <vector>

using namespace::std;

class Solution {
public:
	unordered_set<int> n;
	int longestConsecutive(vector<int> &num) {
		int res = 0;
		for (int i = 0; i < num.size(); i++)n.insert(num[i]);
		unordered_set<int> ::iterator it = n.begin(), it_w;
		while (n.size() != 0){
			it = n.begin();
			int key = *it, work_key = 0, count = 1;
			it_w = n.find(key + 1);
			while (it_w != n.end()){
				work_key = *it_w + 1;
				count++;
				n.erase(*it_w);
				it_w = n.find(work_key);
			}
			it_w = n.find(key - 1);
			while (it_w != n.end()) {
				work_key = *it_w - 1;
				count++;
				n.erase(*it_w);
				it_w = n.find(work_key);
			}
			if (count > res)res = count;
			count = 0;
			n.erase(key);
		}
		return res;
	}
};

int main(){
	int a[] = { 100, 4, 200, 1, 3, 2 };
	vector<int> nums(a, a + sizeof(a) / sizeof(int));
	Solution s;
	int res = s.longestConsecutive(nums);
	return 0;
}