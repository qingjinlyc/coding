#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <queue>
using namespace ::std;
class Solution {
public:
	int ladderLength(string start, string end, unordered_set<string> &dict) {
		vector<string> strs;
		queue<string> q;
		unordered_set<string> already;
		q.push(start);
		already.insert(start);
		int last = 1, cur = 0;
		for (unordered_set<string> ::iterator it = dict.begin(), end = dict.end(); it != end; ++it)strs.push_back(*it);
		int step = 1;
		while (!q.empty()){
			string s = q.front();
			q.pop();
			--last;
			if (onediff(s, end) != -1)return step + 1;
			for (int i = 0; i < strs.size(); ++i){
				if (already.find(strs[i]) == already.end() && onediff(s, strs[i]) != -1){
					q.push(strs[i]);
					already.insert(strs[i]);
					++cur;
				}
			}
			if (last == 0){
				last = cur;
				cur = 0;
				++step;
			}
		}
		return 0;
	}
private:
	int onediff(string a, string b) {
		if (a == "" || b == "")return 0;
		if (a.length() != b.length())return -1;
		int len = a.length(), hasdiff = -1;
		for (int i = 0; i < len; ++i) {
			if (a[i] != b[i]) {
				if (hasdiff != -1)return -1;
				hasdiff = i;
			}
		}
		return hasdiff;
	}
};



