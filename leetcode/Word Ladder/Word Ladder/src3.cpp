#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <queue>
using namespace ::std;
class Solution {
public:
	int ladderLength(string start, string end, unordered_set<string> &dict) {
		queue<string> q;
		unordered_set<string> already;
		unordered_map<string, bool> dup_dict;
		unordered_map<string, bool>::iterator it;
		/*
		for (unordered_set<string> ::iterator it = dict.begin(), end = dict.end(); it != end; ++it){
			dup_dict.insert(pair<string, bool>(*it, false));
		}
		*/
		q.push(start);
//		already.insert(start);
		int last = 1, cur = 0;
		string cs;
		int step = 1;
		while (!q.empty()){
			string s = q.front();
			q.pop();
//			it = dup_dict.find(s);
//			if (it != dup_dict.end())it->second = true;
			--last;
			if (onediff(s, end) != -1)return step + 1;
			for (int i = 0; i < s.length(); ++i){
				for (int j = 'a'; j <= 'z'; ++j){
					if (s[i] == j)continue;
					if ((dict.find((cs = changechar(s, i, j)))) != dict.end() /* && it->second == false */){
						q.push(cs);
						dict.erase(cs);
						++cur;
					}
				}
			}
			if (last == 0){
				last = cur;
				cur = 0;
				++step;
			}
		}
		return 0;
	}
private:
	string changechar(string a, int pos, char c) {
		a[pos] = c;
		return a;
	}
	int onediff(string a, string b) {
		if (a == "" || b == "")return 0;
		if (a.length() != b.length())return -1;
		int len = a.length(), hasdiff = -1;
		for (int i = 0; i < len; ++i) {
			if (a[i] != b[i]) {
				if (hasdiff != -1)return -1;
				hasdiff = i;
			}
		}
		return hasdiff;
	}
};

int main(){
	unordered_set<string> dict({ "hot", "dot", "dog", "lot", "log" });
	Solution s;
	int ret = s.ladderLength("hit", "cog", dict);
	return 0;
}