#include <vector>
#include <string>
#include <unordered_set>
using namespace ::std;
class Solution {
public:
	int ladderLength(string start, string end, unordered_set<string> &dict) {
		vector<string> strs;
		vector<int> already(dict.size(), 0);
		for (unordered_set<string> ::iterator it = dict.begin(), end = dict.end(); it != end; ++it)strs.push_back(*it);
		int min = 0x0fffffff;
		findsteps(start, end, already, 0, min, strs);
		min = (min == 0x0fffffff ? -1 : min);
		return min + 1;
	}
private:
	int onediff(string a, string b) {
		if (a == "" || b == "")return 0;
		if (a.length() != b.length())return -1;
		int len = a.length(), hasdiff = -1;
		for (int i = 0; i < len; ++i) {
			if (a[i] != b[i]) {
				if (hasdiff != -1)return -1;
				hasdiff = i;
			}
		}
		return hasdiff;
	}
	void findsteps(string start, string end, vector<int> &already, int step, int &min, vector<string> &dict) {
		int diffpos = -1, findnew = 0;
		for (int i = 0; i < dict.size(); ++i){
			if (already[i] != 1){
				if ((diffpos = onediff(start, dict[i])) != -1){
					already[i] = 1;
					findsteps(dict[i], end, already, step + 1, min, dict);
					//already[i] = 0;
				}
			}
		}
		if (onediff(start, end) != -1){
			++step;
			if (step < min)min = step;
			return;
		}
	}
};

