#include <vector>
#include <string>
using namespace ::std;
class Solution {
public:
	bool exist(vector<vector<char> > &board, string word) {
		vector<int> lines(board[0].size(), 0);
		vector<vector<int> > used(board.size(), lines);
		int size = board.size() * board[0].size(), wl = word.length();
		bool can = false;
		for (int i = 0; i < board.size(); i++)
			for (int j = 0; j < board[i].size(); j++) {
				can = false;
				if (word.c_str()[0] == board[i][j])
					if (word.length() == 1)return true;
					else{
						used[i][j] = 1;
						can |= e(board, used, word.substr(1), i, j);
						used[i][j] = 0;
					}
					if (can)return true;
			}
		return can;
	}

	bool e(vector<vector<char> > & board, vector<vector<int> > & used, string word, int x, int y) {
		bool can = false;
		if (y != 0 && !used[x][y - 1] && board[x][y - 1] == word.c_str()[0]) {
			if (word.length() == 1)can = true;
			else {
				used[x][y - 1] = 1;
				can |= e(board, used, word.substr(1), x, y - 1);
				used[x][y - 1] = 0;
			}
		}
		if (can)return true;
		if (y != board[0].size() - 1 && !used[x][y + 1] && board[x][y + 1] == word.c_str()[0]) {
			if (word.length() == 1)can = true;
			else {
				used[x][y + 1] = 1;
				can |= e(board, used, word.substr(1), x, y + 1);
				used[x][y + 1] = 0;
			}
		}
		if (can)return true;
		if (x != 0 && !used[x - 1][y] && board[x - 1][y] == word.c_str()[0]) {
			if (word.length() == 1)can = true;
			else {
				used[x - 1][y] = 1;
				can |= e(board, used, word.substr(1), x - 1, y);
				used[x - 1][y] = 0;
			}
		}
		if (can)return true;
		if (x != board.size() - 1 && !used[x + 1][y] && board[x + 1][y] == word.c_str()[0]) {
			if (word.length() == 1)can = true;
			else {
				used[x + 1][y] = 1;
				can |= e(board, used, word.substr(1), x + 1, y);
				used[x + 1][y] = 0;
			}
		}
		return can;
	}
};

int main(){
	vector<vector<char> > board;
	board.push_back({'E', 'D', 'C'});
	board.push_back({ 'F', 'A', 'B'});
	board.push_back({ 'G', 'H', 'I'});

	Solution s;
	bool res = s.exist(board, "ABCDEFGHI");
	return 0;
}