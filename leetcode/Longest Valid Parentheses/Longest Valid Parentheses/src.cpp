#include <vector>
#include <string>

using namespace::std;

class Solution {
public:
	int longestValidParentheses(string s) {
		int len = s.length();
		if (s == "")return 0;
		vector<int> col(len, 0);
		vector<vector<int> > L(len, col);
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++)
			if (j - i == 1 && s[i] == '(' && s[j] == ')')L[i][j] = 2;
			else
				L[i][j] = 0;
		}
		for (int j = 2; j < len; j++)
		for (int i = j - 2; i > -1; i--) {
			if (s[j] == ')' && s[i] == '(') {
				int max = L[i + 1][j] > L[i][j - 1] ? L[i + 1][j] : L[i][j - 1];
				L[i][j] = max;
				if (max == (j - i - 1))L[i][j] += 2;
			}
			else if (s[j] == ')' && s[i] == ')')
				L[i][j] = L[i + 1][j];
			else if (s[j] == '(' && s[i] == '(')
				L[i][j] = L[i][j - 1];
			else
				L[i][j] = L[i + 1][j - 1];
		}
		return L[0][len - 1];
	}
};

