#include <string>
#include <vector>
using namespace::std;

class Solution {
public:
	int longestValidParentheses(string s) {
		int len = s.length();
		vector<int> L(s.length(), 0);
		if (s == "")return 0;
		for (int i = len - 2; i > -1; i--){
			int j = i + L[i + 1] + 1;
			if (s[i] == '(') {
				if (i + L[i + 1] + 1 < s.length()){
					if (s[i + L[i + 1] + 1] == ')')L[i] = L[i + 1] + 2;
					else
						L[i] = L[i + 1];
				}
				else
					L[i] = L[i + 1];
				if (j + 1 < len)L[i] += L[j + 1];
			}
			else{
				L[i] = L[i + 1];
			}
		}
		return L[0];
	}
};

int main(){
	Solution s;
	int res = s.longestValidParentheses("(()))");
	return 0;
}

