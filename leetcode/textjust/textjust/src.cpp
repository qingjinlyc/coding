#include <vector>
#include <string>

using namespace ::std;

class Solution {
public:
	vector<string> fullJustify(vector<string> &words, int L) {
		int len = words.size(), left = 0, right = 0, wl = 0, slots = 0;
		string line;
		vector<string> ret;
		while(left < len) {
			line = "";
			wl = 0;
			while(right < len) {
				if(wl != 0) {
					if(wl + words[right].length() + 1 <= L) {
						wl += (words[right].length() + 1);
						right++;
					}
					else {
						right--;
						break;
					}
				}
				else {
					if(wl + words[right].length() <= L) {
						wl += words[right].length();
						right++;
					}
					else {
						right--;
						break;
					}
				}
			}
			if(right == len)right--;
			if(right == len - 1) {
				while(left < right) {
					line += words[left] + " ";
					left++;
				}
				line += words[left];
				slots = L - wl;
				while(slots) {
					line += " ";
					slots--;
				}
				ret.push_back(line);
				break;
			}
			if(left == right) {
				line += words[left];
				slots = L - wl;
				while(slots) {
					line+= " ";
					slots--;
				}
				left = right + 1;
				right = left;
				ret.push_back(line);
				continue;
			}
			slots = L - wl;
			vector<int> ex = devide(slots, right - left);
			int ind = 0;
			while(left < right) {
				line += (words[left] + " ");
				while(ex[ind]) {
					line += " ";
					ex[ind]--;
				}
				left++;
				ind++;
			}
			line+= words[left];
			ret.push_back(line);

			left = right + 1;
			right = left;
		}
		return ret;
	}
	vector<int> devide(int slots, int n) {
		vector<int> ret(n, 0);
		while(slots) {
			for(int i = 0; i < n && slots; i++, slots--)
				ret[i]++;
		}
		return ret;
	}
};

int main(){
	Solution s;
	string str[] = {"What","must","be","shall","be."};
	vector<string> words(str, str + sizeof(str) / sizeof(str[0]));
	vector<string> ret = s.fullJustify(words, 12);
	return 0;
}