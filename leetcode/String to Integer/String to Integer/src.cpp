#include <iostream>

using namespace ::std;

class Solution {
public:
	int atoi(const char *str) {
		int res = 0, isNeg = 0;
		if (str == NULL)return 0;
		while (!isValid(*str) && *str != 0){
			isNeg |= (*str == '-');
			++str;
		}
		if (*(str - 1) == '0')--str;
		while ((*str == '0' || !isValid(*str)) && *str != 0)++str;
		while (*str != 0){
			if (!isValid(*str))return 0;
			res = res * 10 + toNum(*str);
			++str;
		}
		if (isNeg)res = 0 - res;
		return res;
	}
private:
	bool isValid(const char c){
		return (c <= '9' && c >= '0');
	}
	int toNum(const char c){
		return c - '0';
	}
};

int main(){
	const char * str = "";
	Solution s;
	int res = s.atoi(str);
	return 0;
}