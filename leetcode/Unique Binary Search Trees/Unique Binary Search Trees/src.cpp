#include <unordered_map>
using namespace ::std;


class Solution {
public:
	unordered_map<int, int> m;
	Solution(){
		m.clear();
	}
	int numTrees(int n) {
		if (n == 0)return 0;
		if (n == 1)return 1;
		if (n == 2)return 2;
		unordered_map<int, int> ::iterator it = m.find(n);
		if (it != m.end())return it->second;
		int sum = 0, up;
		if (n % 2 == 0)up = (n - 1) / 2;
		else
			up = (n - 1) / 2 - 1;
		for (int i = 1; i <= up; i++)
			sum += (numTrees(i) * numTrees(n - i - 1));
		sum += numTrees(n - 1);
		sum *= 2;
		int p = numTrees((n - 1) / 2);
		if (n % 2 != 0)sum += p * p;
		m.insert(pair<int, int>(n, sum));
		return sum;
	}
};


int main(){
	Solution s;
	int res = s.numTrees(5);
	return 0;
}