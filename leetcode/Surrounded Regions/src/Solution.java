import java.util.AbstractQueue;
import java.util.LinkedList;
import java.util.Queue;


public class Solution {

	/**
	 * @param args
	 */
	private class Point{
		int x;
		int y;
		Point(int a, int b){
			x = a;
			y = b;
		}
		Point(Point p){
			x = p.x;
			y = p.y;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char board[][] = {{'X'}};
		new Solution().solve(board);
		for(int i = 0; i < board.length; ++i){
			for(int j = 0; j < board[0].length; ++j)
				System.out.print(board[i][j] + "\t");
			System.out.print("\n");
		}
		return;
	}
	public void solve(char[][] board) {
        int i0 = -1, j0 = -1;
        int i = 0, j = 0;
        boolean has = false;
        if(board.length == 0 || board[0].length == 0)return;
        int height = board.length, width = board[0].length;
        for(j = 0; j < width; ++j){
        	if(board[0][j] == 'O')
        		travers(0, j, board);
        }
       	for(i = 0; i < height; ++i){
        	if(board[i][width - 1] == 'O')
        		travers(i, width - 1, board);
        }
   		for(i = 0; i < width; ++i){
        	if(board[height - 1][i] == 'O')
        		travers(height - 1, i, board);
        }

   		for(i = 0; i < height; ++i){
        	if(board[i][0] == 'O')
        		travers(i, 0, board);
        }
        tern('O', 'X', board);
        tern('1', 'O', board);
    }
    private void travers(int i, int j, char [][] board){
    	Queue<Point> q = new LinkedList<Point>();
    	q.offer(new Point(i, j));
    	while(!q.isEmpty()){
    		Point p = new Point(q.poll());
    		board[p.x][p.y] = '1';
    		i = p.x;
    		j = p.y;
	    	if(i + 1 < board.length && board[i + 1][j] == 'O'){
	    		board[i + 1][j] = '1';
	    		q.offer(new Point(i + 1, j));
	    	}
	    	if(i - 1 > -1 && board[i - 1][j] == 'O'){
	    		board[i - 1][j] = '1';
	    		q.offer(new Point(i - 1, j));
	    	}
	    	if(j + 1 < board[0].length && board[i][j + 1] == 'O'){
	    		board[i][j + 1] = '1';
	    		q.offer(new Point(i, j + 1));
	    	}
	    	if(j - 1 > -1 && board[i][j - 1] == 'O'){
	    		board[i][j - 1] = '1';
	    		q.offer(new Point(i, j - 1));
	    	}
    	}
    }
    
    private void tern(char from, char to, char [][] board) {
    	int h = board.length, w = board[0].length;
    	for(int i = 0; i < h; ++i)
    		for(int j = 0; j < w; ++j){
    			if(board[i][j] == from)board[i][j] = to;
    		}
    }
}
