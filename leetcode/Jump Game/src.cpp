#include <string.h>
using namespace :: std;
class Solution {
public:
    bool canJump(int A[], int n) {
        int *al = new int[n];
        memset(al, 0, 4 * n);
        return __canjump(A, 0, al, n);
    }
    
    bool __canjump(int A[], int pos, int *al, int n){
        if(pos == n - 1 || pos + A[pos] >= n - 1)return true;
        al[pos] = 1;
        bool ret = false;
        for(int i = 1; i <= A[pos]; ++i) {
            if(pos + i <= n - 1 && !al[pos + i])ret = ret || __canjump(A, pos + i, al, n);
        }
        return ret;
    }
};

int main(){
	int A[] = {2,3,1,1,4};
	bool ret = Solution().canJump(A, 5);
	return 0;
}
