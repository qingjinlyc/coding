class Solution {
public:
    int removeDuplicates(int A[], int n) {
        int ret = n;
        for(int i = 0; i < ret; ++i) {
            int j = i;
            for(j = i + 1; j < ret && A[j] == A[j - 1]; ++j);
            if(j - i > 2){
                int h = j - i - 2;
                int j_ = j;
                for(int k = j - (h); j < ret; ++k, ++j) {
                    A[k] = A[j];
                }
                ++i;
                ret = (ret - (h));
            }
			else i = j - 1;
        }
        return ret;
    }
};

int main () {
	int A[8] = {1,1,1,2,2,2,3,3};
	Solution s;
	int ret = s.removeDuplicates(A, 8);
	return 0;
}