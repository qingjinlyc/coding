import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;


public class Solution {

	/**
	 * @param args
	 */
	int a;
	int b;
	public Solution(int a, int b){
		this.a = a;
		this.b = b;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num[] = {3, 5, 1, 10, 31};
		ArrayList<Solution> as = new ArrayList<Solution>();
		as.add(new Solution(1, 3));
		as.add(new Solution(3, 1));
		as.add(new Solution(-1, 5));
		as.add(new Solution(134, 31));
		Arrays.sort(as.toArray());
		return;
	}
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	}
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<Integer>();
        Stack<TreeNode> s = new Stack<TreeNode>();
        if(root == null)return res;
        s.push(root);
        while(!s.isEmpty()){
        	TreeNode t = s.peek();
        	if(t.left == null && t.right == null){
        		res.add(t.val);
        		s.pop();
        	}
        	else if(t.right != null)
        		s.push(t.right);
        	else
        		s.push(t.left);
        }
        return res;
    }
}
