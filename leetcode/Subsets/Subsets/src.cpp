class Solution {
public:
	bool compare(const int & a, const int & b) {
		return (b - a);
	}
	vector<vector<int> > subsets(vector<int> &S) {
		vector<vector<int> > res;
		vector<int> oneres;
		if (S.size() == 0)return res;
		int size = S.size(), max = 1;
		max = max << size;
		std::sort(S.begin(), S.end());
		for (int i = 0; i < max; ++i) {
			int bits = i;
			for (int j = 0; j < size; ++j) {
				if (bits & 1)oneres.push_back(S[j]);
				bits = bits >> 1;
			}
			res.push_back(oneres);
			oneres.clear();
		}
		return res;
	}
};