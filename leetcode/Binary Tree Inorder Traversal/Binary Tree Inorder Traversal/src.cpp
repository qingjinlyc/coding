#include <vector>
#include <stack>
using namespace::std;

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	vector<int> inorderTraversal(TreeNode *root) {
		vector<int> res;
		if (!root)return res;
		stack<TreeNode *> s;
		while (root->left){
			s.push(root);
			root = root->left;
		}
		if (root->right)s.push(root);
		else
			res.push_back(root->val);
		while (!s.empty()) {
			TreeNode * n = s.top();
			s.pop();
			res.push_back(n->val);
			n = n->right;
			if (!n)continue;
			while (n && n->left){
				s.push(n);
				n = n->left;
			}
			if (!n->right)res.push_back(n->val);
			else
				s.push(n);
		}
		return res;
	}
};