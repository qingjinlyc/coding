
public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A[] = {1,3,5,6};
		int ret = new Solution().searchInsert(A, 2);
		System.out.println(ret);
	}
	public int searchInsert(int[] A, int target) {
		int i = 0;
        for(i = A.length - 1; i > -1 && A[i] > target; --i);
        if(i > -1 && A[i] == target)--i;
        return i + 1;
    }
}
