#include <stdio.h>
using namespace::std;

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	const int min = -99999;
	int maxPathSum(TreeNode *root) {
		if (!root)return 0;
		int max_len = 0;
		return maxsum(root, max_len);
	}
	int maxsum(TreeNode * root, int & max_len) {
		if (!root->left && !root->right) {
			if (root->val > 0)max_len = root->val;
			else
				max_len = 0;
			return root->val;
		}
		int left_max = min, right_max = min, left_len = min, right_len = min;
		if (root->left)left_max = maxsum(root->left, left_len);
		if (root->right)right_max = maxsum(root->right, right_len);
		int cross = ((right_len > 0) ? right_len : 0) + ((left_len > 0) ? left_len : 0) + root->val;
		int max_root = (left_max > right_max) ? (left_max > cross ? left_max : cross) : (right_max > cross? right_max:cross);
		max_len = root->val + ((left_len > right_len)?left_len:right_len);
		if (max_len < 0)max_len = 0;
		return max_root;
	}
};

int main(){
	TreeNode root(-1);
	root.left = new TreeNode(8);
	root.right = new TreeNode(2);
	root.left->right = new TreeNode(-9);
	root.right->left = new TreeNode(0);
	root.right->left->right = new TreeNode(-3);
	root.right->left->right->left = new TreeNode(-9);
	root.right->left->right->left->right = new TreeNode(2);
	Solution s;
	int res = s.maxPathSum(&root);
	return 0;
}