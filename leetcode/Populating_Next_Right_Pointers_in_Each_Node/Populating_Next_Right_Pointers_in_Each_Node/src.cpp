#include <iostream>
using namespace ::std;
/**
* Definition for binary tree with next pointer.
*/
struct TreeLinkNode {
 int val;
 TreeLinkNode *left, *right, *next;
 TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};

class Solution {
public:
	TreeLinkNode * parent;
	Solution(){
		parent = NULL;
	}
	void connect(TreeLinkNode *root) {
		if (root == NULL) return;
		TreeLinkNode * work = root, *first = root;
		while (work->left && work->right){
			work->left->next = work->right;
			if (work->next){
				work->right->next = work->next->left;
				work = work->next;
				continue;
			}
			else{
				work = first->left;
				first = work;
			}
		}
	}
};