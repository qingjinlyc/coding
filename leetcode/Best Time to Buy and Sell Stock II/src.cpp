class Solution {
public:
    int maxProfit(vector<int> &prices) {
        int dec = 1;
        int min = 0, count = 0;
        for(int i = 1; i < prices.size(); i++){
            if(prices[i] > prices[i - 1]){
                if(dec){
                   min = i - 1;
                   dec = 0;
                }
            }
            else if(prices[i] < prices[i - 1]){
                if(!dec){
                    count += (prices[i - 1] - prices[min]);
                    dec = 1;
                    min = i;
                }
                else
                    min = i;
            }
            else{
                dec = 0;
            }
            if(i == prices.size() - 1){
                if(!dec)count += (prices[i] - prices[min]);
            }
        }
        return count;
    }
};