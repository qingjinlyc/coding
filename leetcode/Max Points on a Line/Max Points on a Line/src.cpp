#include <vector>
#include <algorithm>
using namespace::std;

/**
* Definition for a point.
*/
struct Point {
    int x;
    int y;
    Point() : x(0), y(0) {}
    Point(int a, int b) : x(a), y(b) {}
};
struct V {
	int a;
	int b;
	V() : a(0), b(0) {}
	V(int x, int y) : a(x), b(y) {}
};
int X(V v1, V v2) {
	return v1.a * v2.b - v1.b * v2.a;
}
struct CardComparer
{
public:
	bool operator() (V v1, V v2) {
		int res = X(v1, v2);
		return (res > 0);
	}
};
class Solution {
public:
	int maxPoints(vector<Point> &points) {
		if (points.size() == 0)return 0;
		if (points.size() == 1)return 1;
		int max = -1;
		vector<V> vs;
		for (int i = 0; i < points.size(); i++) {
			int same = 0;
			vs.clear();
			for (int j = 0; j < points.size(); j++) {
				if (j == i)continue;
				V av(points[j].x - points[i].x, points[j].y - points[i].y);
				vs.push_back(av);
			}
			sort(vs.begin(), vs.end(), CardComparer());
			int count = 0, max_ = count, flag = 0;
			for (; flag < vs.size() && vs[flag].a == 0 && vs[flag].b == 0; flag++);
			if (flag == vs.size()){
				if (flag >= max)max = flag + 1;
				continue;
			}
			for (int j = flag; j < vs.size(); j++){
				if (vs[j].a == 0 && vs[j].b == 0){
					same++;
					if (j == vs.size() - 1) {
						if (count > max_)max_ = count;
					}
					continue;
				}
				if (X(vs[j], vs[flag]) == 0)count++;
				if (X(vs[j], vs[flag]) != 0 || j == vs.size() - 1) {
					if (count > max_)max_ = count;
					count = 1;
					flag = j;
				}
			}
			max_ += (same + 1);
			if (max_ > max)max = max_;
		}
		return max;
	}
};


int main(){
	vector<Point> p;
	Point p1(0, 0), p2(0, 0), p3(0, 0), p4(1, 1), p5(1, 2), p6(2, 4);
	p.push_back(p1);
	p.push_back(p2);
//	p.push_back(p3);
//	p.push_back(p4);
//	p.push_back(p5);
//	p.push_back(p6);
	Solution s;
	int res = s.maxPoints(p);
	return 0;
}