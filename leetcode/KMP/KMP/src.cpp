#include <string>
#include <vector>
using namespace::std;

class Solution {
public:
	char *strStr(char *haystack, char *needle){
		string T(haystack), P(needle);
		int len_t = T.length(), len_p = P.length();
		vector<int> res, next(len_p, 0);
		if (len_p == 0 && len_t == 0)return "";
		if (len_p == 0)return haystack;
		for (int i = 0; i < len_p; i++) {
			int k = i, j = 1;
			while (k){
				if (P.substr(0, k) != P.substr(j, k)) {
					j++;
					k--;
				}
				else
					break;
			}
			next[i] = k;
		}
		int j = 0;
		for (int i = 0; i < len_t; i++){
			if (T[i] != P[j]){
				if (j != 0){
					j = next[j - 1];
					i--;
				}
			}
			else if (j == len_p - 1){
				return haystack + i - j;
				j++;
			}
			else
				j++;
		}
		return NULL;
	}
};

int main(){
	Solution s;
	string T, P;
	cin >> T >> P;
	vector<int> res = s.findPattern(T, P);
	for (int i = 0; i < res.size(); i++)cout << res[i] << "\t";
	cout << endl;
	return 0;
}