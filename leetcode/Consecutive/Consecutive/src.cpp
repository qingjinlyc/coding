#include <iostream>
#include <vector>


using namespace ::std;

class Solution {
public:
	int longestConsecutive(vector<int> &num) {
		const long int lenp = 100000, lenm = lenp;
		int nump[lenp], numm[lenm];
		long int count = 0, max = 0;
		for (long int i = 0; i < lenp; i++){
			nump[i] = 0;
			numm[i] = 0;
		}
		for (int i = 0; i < num.size(); i++){
			if (num[i] >= 0)nump[num[i]] = 1;
			else
				numm[-num[i]] = 1;
		}
		for (long int i = lenm - 1; i > 0; i--){
			if (numm[i])count++;
			else{
				if (count > max)max = count;
				count = 0;
			}
		}
		for (long int i = 0; i < lenp; i++) {
			if (nump[i])count++;
			else{
				if (count > max)max = count;
				count = 0;
			}
		}
		return max;
	}
};

int main() {
	Solution s;
	vector<int> num;
	num.push_back(0);
	int ret = s.longestConsecutive(num);
	return 0;
}