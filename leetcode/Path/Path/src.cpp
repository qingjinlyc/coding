#include <iostream>
#include <vector>
#include <string>
using namespace ::std;
class Solution {
public:
	string simplifyPath(string path) {
		vector<string> sim;
		string dir, ret;
		if(path == "/")return "/";
		for(int i = 0, len = path.length(); i < len; i++) {
			if(i == len - 1 && path[i] == '/')break;
			if(path[i] == '/') {
				for(i++; i < len && path[i] == '/'; i++);
				int j = i;
				for(; j < len && path[j] != '/'; j++);
				dir = path.substr(i, j - i);
				if(dir == ".." && sim.size() > 0)
					sim.pop_back();
				else if(dir != "." && dir != ".." && dir != "")
					sim.push_back(dir);
				i = j - 1;
			}
		}
		if(sim.size() == 0)return "/";
		int i = 0;
		for(i = 0; i < sim.size(); i++)
			ret += ("/" + sim[i]);
		return ret;
	}
};

int main(){
	Solution s;
	string ret = s.simplifyPath("/...");
	return 0;
}