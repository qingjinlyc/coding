#include <vector>
using namespace :: std;
/**
 * Definition for binary tree
 */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        return __build(preorder, 0, preorder.size() - 1, inorder, 0, inorder.size() - 1);
    }
    
private:
    TreeNode *__build(vector<int> &pre, int s1, int e1, vector<int> &in, int s2, int e2) {
        if(s1 == e1) {
            return new TreeNode(pre[s1]);
        }
        if(e1 < s1 || e2 < s2)return NULL;
        int i = 0;
        for(i = s2; i <= e2 && in[i] != pre[s1]; ++i);
        TreeNode *root = new TreeNode(pre[s1]);
        root->left = __build(pre, s1 + 1, s1 + i - s2, in, s2, i - 1);
        root->right = __build(pre, s1 + i - s2 + 1, e2, in, i + 1, e2);
        return root;
    }
};

int main () {
	vector<int> pre, in;
	pre.push_back(1);
	pre.push_back(2);
	pre.push_back(3);

	in.push_back(2);
	in.push_back(3);
	in.push_back(1);

	Solution s;
	TreeNode *root = s.buildTree(pre, in);
	return 0;
}