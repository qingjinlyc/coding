import java.util.ArrayList;


public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "aaaaa";
		System.out.println(new Solution().minCut(s));
	}
    public int minCut(String s) {
        boolean isp[][] = new boolean[s.length() + 1][s.length() + 1];
        int minl[] = new int[s.length() + 1];
        for(int i = 0; i < s.length(); ++i)
        	for(int j = 0; j <s.length(); ++j)isp[i][j] = false;
        for(int i = 0; i < s.length() + 1; ++i){
        	isp[i][i] = true;
        	minl[i] = s.length() - i - 1;
        }
        for(int i = s.length() - 1; i > -1; --i){
        	for(int j = i; j < s.length(); ++j) {
        		isp[i][j] = i == j || s.charAt(i) == s.charAt(j) && (i == j - 1 || isp[i + 1][j - 1]);
        		if(isp[i][j] && minl[j + 1] + 1 < minl[i])
        			minl[i] = minl[j + 1] + 1;
        	}
        }
        return minl[0];
    }
}
