#include <iostream>
#include <map>
using namespace ::std;



class Solution {
public:
	int lengthOfLongestSubstring(string s) {
		if (s == "")return 0;
		int start = 0, max = -1;
		map<char, int> h;
		map<char, int> ::iterator it;
		h.insert(pair<char , int>(s[0], 0));
		for (int i = 1; i < s.size(); ++i){
			it = h.find(s[i]);
			if (it != h.end()){
				int len = i - start;
				if (len > max)max = len;
				start = it->second + 1;
//				i = start;
				h.clear();
				for (int j = start; j <= i; ++j)h.insert(pair<char, int>(s[j], j));
			}
			else{
				h.insert(pair<char, int>(s[i], i));
			}
		}
		return max;
	}
};

int main(){
	string s = "abcabcbb";
	Solution so;
	int res = so.lengthOfLongestSubstring(s);
	return 0;
}