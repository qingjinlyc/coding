#include <iostream>

using namespace ::std;

class Solution {
public:
	bool isPalindrome(int x) {
		if (x < 0)return false;
		int len = 0, temp = x, times = 1;;
		for (; temp; ++len){
			temp /= 10;
		}
		for (int i = 0; i < (len - 1); ++i)times *= 10;
		for (int i = 0; i < (len + 1) / 2; ++i){
			int last = x % 10;
			x = x - times * last;
			x /= 10;
			if (x < 0)return false;
			times /= 100;
		}
		if (x)return false;
		return true;
	}
};

int main(){
	int x = 112211;
	Solution s;
	bool res = s.isPalindrome(x);
	return 0;
}