class Solution {
public:
	int sqrt(int x) {
		if (x == 0 || x == 1)return x;
		int upper = 46340, start = 1, end = (x > upper ? upper : x), mid = start + (end - start) / 2;
 		while (end - start > 1) {
			if (mid * mid > x) {
				end = mid - 1;
			}
			else if (mid * mid < x) {
				start = mid + 1;
			}
			else
				return mid;
			mid = start + (end - start) / 2;
		}
		if (end * end >= x)
		{
			while (end * end > x)--end;
			return end;
		}
		else
			return end;
	}
};

int main(){
	Solution s;
	int ret = s.sqrt(2147483647);
	return 0;
}