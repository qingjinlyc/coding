class Solution {
public:
	vector<int> twoSum(vector<int> &numbers, int target) {
		unordered_map<int, int> hm;
		vector<int> res;
		for (int i = 0; i < numbers.size(); i++)
		if (hm.find(numbers[i]) == hm.end())
			hm.insert(pair<int, int>(numbers[i], i + 1));
		for (int i = 0; i < numbers.size(); i++) {
			int tofind = target - numbers[i];
			unordered_map<int, int>::iterator it;
			if ((it = hm.find(tofind)) != hm.end() && it->second != i + 1) {
				int index = it->second;
				res.push_back(i + 1 < index ? i + 1 : index);
				res.push_back(i + 1 < index ? index : i + 1);
				return res;
			}
		}
		return res;
	}
};