#include <queue>
using namespace::std;


/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	void flatten(TreeNode *root) {
		queue<TreeNode *> q;
		previsit(root, q);
		TreeNode * w = root;
		if (!q.empty())q.pop();
		while (!q.empty()){
			TreeNode * node = q.front();
			q.pop();
			w->right = node;
			w = w->right;
		}
	}
	void previsit(TreeNode * n, queue<TreeNode *> & q){
		if (!n)return;
		q.push(n);
		previsit(n->left, q);
		previsit(n->right, q);
		n->left = NULL;
		n->right = NULL;
	}
};

int main(){
	TreeNode root(1);
	root.left = new TreeNode(2);
	Solution s;
	s.flatten(&root);
	return 0;
}