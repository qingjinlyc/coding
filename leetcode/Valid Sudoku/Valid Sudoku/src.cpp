#include <iostream>
#include <vector>
using namespace ::std;

class Solution {
public:
	bool isValidSudoku(vector<vector<char> > &board) {
		bool res = false;
		for (int i = 0; i < 9; ++i)res = validline(i, board);
		if (res == false)return false;
		for (int i = 0; i < 9; ++i)res = validcol(i, board);
		if (res == false)return false;
		for (int i = 0; i < 9; i += 3){
			for (int j = 0; j < 9; j += 3){
				res = validblock(i, j, board);
				if (res == false)return false;
			}
		}
		return true;
	}
private:
	bool has[9];
	int ch2int(char c){
		return c - '1';
	}

	bool validline(int line, vector<vector<char> > &board) {
		for (int i = 0; i < 9; ++i)has[i] = false;
		for (int i = 0; i < 9; ++i){
			if (board[line][i] == '.')continue;
			int pos = ch2int(board[line][i]);
			if (has[pos])return false;
			has[pos] = true;
		}
		return true;
	}

	bool validcol(int line, vector<vector<char> > &board) {
		for (int i = 0; i < 9; ++i)has[i] = false;
		for (int i = 0; i < 9; ++i){
			if (board[i][line] == '.')continue;
			int pos = ch2int(board[i][line]);
			if (has[pos])return false;
			has[pos] = true;
		}
		return true;
	}

	bool validblock(int line, int col, vector<vector<char> > &board) {
		for (int i = 0; i < 9; ++i)has[i] = false;
		for (int i = line; i < line + 3; ++i){
			for (int j = col; j < col + 3; ++j){
				if (board[i][j] == '.')continue;
				int pos = ch2int(board[i][j]);
				if (has[pos])return false;
				has[pos] = true;
			}
		}
		return true;
	}
};