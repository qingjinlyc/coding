
public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A[] = {0,4,4,0,4,4,4,0,2};
		int ret = new Solution().removeElement(A, 4);
		for(int i = 0; i < ret; ++i)System.out.print(A[i] + "\t");
		System.out.println("\n" + ret);
	}
	public int removeElement(int[] A, int elem) {
		int len = A.length;
		int last = len - 1, index = 0;
		int i = 0;
		for(; index < A.length; ++index, ++i) {
			if(A[index] == elem)--len;
		}
		for(index = 0; index < A.length && i > -1; ++index, --i) {
			if(A[index] == elem){
				while(last > -1 && A[last] == elem)--last;
				if(last == -1)return len;
				A[index] = A[last--];
			}
		}
		return len;
    }
}
