#include <iostream>
#include <queue>
using namespace ::std;

/**
* Definition for singly-linked list.
*/
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


class Solution {
public:
	TreeNode *sortedListToBST(ListNode *head) {
		if (!head)return NULL;
		int len = getListLen(head);
		TreeNode * res = buildTree(len);
		fillTree(res, &head);
		return res;
	}

private:
	int getListLen(ListNode *head){
		int len = 0;
		while (head){
			head = head->next;
			++len;
		}
		return len;
	}
private:
	TreeNode * buildTree(int len){
		if (len == 0) return NULL;
		queue<TreeNode *> q;
		TreeNode * root = new TreeNode(0);
		q.push(root);
		TreeNode * work = NULL;
		--len;
		while (len && !q.empty()){
			work = q.front(); 
			q.pop();
			if (len){
				work->left = new TreeNode(0);
				--len;
				q.push(work->left);
			}
			if (len){
				work->right = new TreeNode(0);
				--len;
				q.push(work->right);
			}
		}
		return root;
	}

	void fillTree(TreeNode * tn, ListNode ** ln){
		if (!tn->left && !tn->right){
			tn->val = (*ln)->val;
			*ln = (*ln)->next;
			return;
		}
		if (tn->left){
			fillTree(tn->left, ln);
		}
		tn->val = (*ln)->val;
		*ln = (*ln)->next;
		if (tn->right){
			fillTree(tn->right, ln);
		}
	}
};

int main(){
	ListNode * head = new ListNode(1);
	head->next = new ListNode(3);
	Solution s;
	TreeNode * res = s.sortedListToBST(head);
	return 0;
}