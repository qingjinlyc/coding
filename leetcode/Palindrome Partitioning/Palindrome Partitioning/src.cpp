#include <unordered_map>
#include <string>
#include <vector>
using namespace::std;


class Solution {
public:
	unordered_map<string, bool> canpart;
	Solution(){
		canpart.clear();
	}
	vector<vector<string>> partition(string s) {
		vector<vector<string> > res;
		vector<string> p;
		part(s, p, res);
		return res;
	}
	bool isPalindrome(string s) {
		if (s == "")return false;
		int len = s.length();
		int i = 0, j = len - 1;
		while (i <= j) {
			if (s[i] == s[j]){
					i++;
					j--;
					continue;
				}
			else return false;
		}
		return true;
	}

	bool part(string str, vector<string> &p, vector<vector<string> > & v){
		if (str == ""){
			v.push_back(p);
			return true;
		}
		bool can = false, cana = false, res = false;
		unordered_map<string, bool> :: iterator it;
		for (int i = 1; i <= str.length(); i++){
			string s = str.substr(0, i);
			it = canpart.find(s);
			if (it != canpart.end())can = it->second;
			else{
				can = isPalindrome(s);
				canpart.insert(pair<string, bool>(s, can));
			}
			if (can){
				p.push_back(s);
				cana = part(str.substr(i), p, v);
				res |= cana;
				p.pop_back();
			}
		}
		return res;
	}
};

int main(){
	Solution s;
	vector<vector<string> > res = s.partition("abbab");
	return 0;
}