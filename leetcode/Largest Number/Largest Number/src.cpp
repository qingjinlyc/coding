#include <string>
#include <vector>
#include <algorithm>
using namespace std;
int compare(string n1, string n2) {
	int s1 = n1.size(), s2 = n2.size();
	if (s1 > s2)return 1;
	else if (s1 < s2)return 0;
	else {
		int i = 0;
		while (i < s1) {
			if (n1[i] > n2[i])return 1;
			else if (n1[i] <= n2[i])return 0;
			else
				++i;
		}
	}
}

class Solution {
public:
	string largestNumber(vector<int> &num) {
		if (num.size() == 0)return "";
		string s;
		std::sort(num.begin(), num.end(), comp());
		for (int i = 0; i < num.size(); ++i)s += to_string(num[i]);
		return s;
	}
private:

	struct comp{
		bool operator () (int n1, int n2) {
			return compare(to_string(n1), to_string(n2));
		}
	};
};

int main(){
	vector<int> num = { 10,2 };
	Solution s;
	string ret = s.largestNumber(num);
	return 0;
}