#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

using namespace ::std;

class Solution {
private:
	int compare(const void * a, const void * b){
		string x = *((string *)a), y = *((string*)b);
		return strcmp(x.c_str(), y.c_str());
	}
public:
	string longestCommonPrefix(vector<string> &strs) {
		if (strs.size() == 0)return "";
		if (strs.size() == 1)return strs[0];
//		sort(strs.begin(), strs.end());
		string res = "";
		for (int i = 1; ; ++i){
			for (int j = 0; j < strs.size() - 1; ++j){
				if (i > strs[j].length() || i > strs[j + 1].length())return strs[0].substr(0, i - 1);
				if (strs[j][i - 1] != strs[j + 1][i - 1])return strs[0].substr(0, i - 1);
			}
		}
		return res;
	}
};

int main(){
	string test[5] = { "lc", "lcl"};
	vector<string> s(test, test + sizeof(test)/ sizeof(string));
	Solution s0;
	string res = s0.longestCommonPrefix(s);
	return 0;
}