#include <vector>
#include <set>
#include <algorithm>
using namespace ::std;
class Solution {
public:
	vector<vector<int> > subsetsWithDup(vector<int> &S) {
		set<vector<int> > s;
		vector<vector<int> > res;
		vector<int> onesub;
		if (S.size() == 0)return res;
		sort(S.begin(), S.end());
		int pos = 1 << S.size(), fix;
		for (int i = 0; i < pos; ++i) {
			for (int j = 0, fix = i; j < S.size(); ++j, fix = fix >> 1) {
				if (fix >> 1 & 1)onesub.push_back(S[j]);
			}
			if (s.find(onesub) == s.end()) {
				res.push_back(onesub);
				s.insert(onesub);
			}
			onesub.clear();
		}
		return res;
	}
};

int main(){
	vector<int> input;
	input.push_back(0);
	Solution s;
	vector<vector<int> > res = s.subsetsWithDup(input);
	return 0;
}