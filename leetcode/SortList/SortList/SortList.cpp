#include <iostream>
using namespace :: std;


//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
 
class Solution {
public:
    ListNode *sortList(ListNode *head) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        int i = 0;
		ListNode * work = head;
		while(work){
			i++;
			work = work->next;
		}
		return sort(head, i);
    }

	ListNode * sort(ListNode * head, int len){
		int i = 1;
		ListNode * work = head, *second = NULL;
		if(len < 2)return head;
		for(; i < len / 2; i++)work = work->next;
		second = work->next;
		work->next = NULL;
		head = sort(head, len / 2);
		second = sort(second, len - len / 2);
		return merge(head, second);
	}

	ListNode * merge(ListNode * a, ListNode * b){
		ListNode * ind1 = a, * ind2 = b, *last = NULL, *res = NULL;
		res = (ind1->val > ind2->val)? ind2: ind1;
		while(ind1 && ind2){
			while(ind1 && ind2 && ind1->val > ind2->val){
				last = ind2;
				ind2 = ind2->next;
			}
			if(last != NULL){
				last->next = ind1;
			}
			while(ind1 && ind2 && ind1->val <= ind2->val){
				last = ind1;
				ind1 = ind1->next;
			}
			last->next = ind2;
		}
		if(ind1 == NULL)last->next = ind2;
		if(ind2 == NULL)last->next = ind1;
		return res;
	}
};

int main(){
	ListNode head(3);
	head.next = new ListNode(4);
	head.next->next = new ListNode(1);
	Solution s;
	ListNode * res = s.sortList(&head);
	return 0;
}