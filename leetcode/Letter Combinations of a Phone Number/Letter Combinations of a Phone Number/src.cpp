#include <iostream>
#include <string>
#include <vector>

using namespace ::std;


class Solution {
private:
	string strs[10];
	int ch2int(char c){
		return c - '0';
	}

	void toStr(string digits, string & s, vector<string> & res) {
		if (digits == ""){
			res.push_back(s);
			return;
		}
		int pos = ch2int(digits[0]);
		for (int i = 0; i < strs[pos].length(); ++i){
			string temp = s;
			s += strs[pos][i];
			toStr(digits.substr(1), s, res);
			s = temp;
		}
	}
public:
	Solution(){
		strs[1] = "";
		strs[2] = "abc";
		strs[3] = "def";
		strs[4] = "ghi";
		strs[5] = "jkl";
		strs[6] = "mno";
		strs[7] = "pqrs";
		strs[8] = "tuv";
		strs[9] = "wxyz";
		strs[0] = " ";
	}
	vector<string> letterCombinations(string digits) {
		vector<string> res;
		string s;
		toStr(digits, s, res);
		return res;
	}
};

int main(){
	string digits = "23";
	Solution s;
	vector<string> res = s.letterCombinations(digits);
	return 0;
}