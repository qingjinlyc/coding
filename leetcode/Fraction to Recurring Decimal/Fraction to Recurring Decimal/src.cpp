#include <map>
#include <string>

using namespace :: std;


class Solution {
public:
    string fractionToDecimal(int numerator, int denominator) {
        string ret = "";
        map<long long, long long> mores;
		long long sym = ((numerator < 0 && denominator > 0 || numerator > 0 && denominator < 0) ? 1 : 0);
        long long more = (numerator < 0? -numerator : numerator);
        long long de = (denominator < 0? -(long long)denominator : denominator);
        long long hasdot = 0;
        long long pos = -1;
        map<long long, long long>::iterator it;
		char buf[100];
        while(more) {
            while(more < de) {
				if(hasdot){
					ret += to_string(more / de);
					++pos;
				}
				else{
					ret += to_string(more / de);
					++pos;
					ret += ".";
					++pos;
					hasdot = true;
				}
				it = mores.find(more);
				if(it == mores.end()){
					if(ret[pos] == '.')mores.insert(pair<long long, long long>(more, pos - 1));
					else
						mores.insert(pair<long long, long long>(more, pos));
				}
				else {
					ret.insert(it->second, "(");
					ret[ret.size() - 1] = ')';
					return ret;
				}
				more %= de;
				more *= 10;
			}
			if(hasdot){
				ret += to_string(more / de);
				++pos;
			}
			else{
				if(more % de == 0)return to_string(more / de);
				ret += to_string(more / de);
				++pos;
				ret += ".";
				++pos;
				hasdot = true;
			}
			it = mores.find(more);
			if(it == mores.end()){
				if(ret[pos] == '.')mores.insert(pair<long long, long long>(more, pos + 1));
				else
					mores.insert(pair<long long, long long>(more, pos));
			}
			else {
				ret.insert(it->second, "(");
				ret[ret.size() - 1] = ')';
				break;
			}
			more = more % de;
			more *= 10;
		}
		if(sym)ret.insert(0, "-");
        return ret;
    }
};

int main() {
	Solution s;
	string ret = s.fractionToDecimal(1, 13);
	return 0;
}