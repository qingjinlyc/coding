#include <iostream>

using namespace ::std;

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	int minDepth(TreeNode *root) {
		int min = 99999999;
		if (!root)return 0;
		preVisit(root, min, 0);
		return min;
	}
private:
	void preVisit(TreeNode * node, int & min, int dep){
		if (!node->left && !node->right){
			++dep;
			if (dep < min)min = dep;
			return;
		}
		if(node->left)preVisit(node->left, min, dep + 1);
		if(node->right)preVisit(node->right, min, dep + 1);
	}
};