#include <iostream>
#include <list>
#include <unordered_map>
using namespace::std;

typedef struct node{
    int key;
    int value;
	node * prev;
    node * next;
}Node;

class link{
	Node * head;
	Node * tail;
	int size;
public:
	link(){
		head = NULL;
		tail = NULL;
		size = 0;
	}

	void push_back(Node * n){
		if(head == NULL){
			head = n;
			tail = n;
			n->prev = NULL;
			n->next = NULL;
		}
		else{
			tail->next = n;
			n->prev = tail;
			n->next = NULL;
			tail = n;
		}
	}

	void MoveToTail(Node * n){
		if(n->next == NULL)return;
		if(n->prev == NULL){
			head = n->next;
			n->next->prev = NULL;
		}
		else{
			n->prev->next = n->next;
			n->next->prev = n->prev;
		}

		tail->next = n;
		n->prev = tail;
		n->next = NULL;
		tail = n;
	}

	void RemoveNode(Node * n){
		if(n->prev == NULL){
			head = n->next;
			if(n->next != NULL)
				n->next->prev = NULL;
			else
				tail = NULL;
			free(n);
		}
		else{
			n->prev->next = n->next;
			if(n->next != NULL)
				n->next->prev = n->prev;
			free(n);
		}
	}

	Node * GetHead(){
		return head;
	}
};
class LRUCache{
public:
    int capacity;
    int count;
    link datas;
	unordered_map<int, Node *> maps;

    LRUCache(int capacity) {
        this->capacity = capacity;
        count = 0;
    }
    
    int get(int key) {
		node * n;
		try{
			n = maps.at(key);
		}catch(out_of_range e){
			return -1;
		}
        datas.MoveToTail(n);
        return n->value;
    }
    
    void set(int key, int value) {
        Node * n;
		
		try{
			n = maps.at(key);
		}catch(out_of_range e){
			if(this->count == capacity){
				n = datas.GetHead();
				maps.erase(n->key);
				datas.RemoveNode(n);
				count--;
			}
			Node * newdata = new Node();
			newdata->key = key;
			newdata->value = value;
			newdata->next = NULL;
			datas.push_back(newdata);
			maps.insert(pair<int, Node *>(key, newdata));
			count++;
			return;
		}
        n->value = value;
        datas.MoveToTail(n);
    }
};

int main(){
	LRUCache l(3);
	l.set(1,1);
	l.set(2,2);
	l.set(3,3);
	l.set(4,4);
	l.get(4);
	l.get(3);
	l.get(2);
	l.get(1);
	l.set(5,5);
	l.get(1);
	l.get(2);
	l.get(3);
	l.get(4);
	l.get(5);
	return 0;
}