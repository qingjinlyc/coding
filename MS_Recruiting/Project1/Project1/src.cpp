#include <iostream>
#include <vector>
#include <string>
using namespace ::std;

bool isValid(char c){
	return (c <= '9' && c >= '0' || c <= 'z' && c >= 'a');
}
int getPos(char c){
	if (c <= '9' && c >= '0'){
		return c - '0';
	}
	else{
		return c - 'a' + 10;
	}
}
char getCha(int i){
	if (i < 10 && i > -1){
		return '0' + i;
	}
	else{
		return 'a' + i - 10;
	}
}
int main(){
	string input;
	vector<int> ch(36, 0);
	cin >> input;
	while (input != ""){
		int i = 0;
		for (; i < input.size() && isValid(input[i]); ++i){
			++ch[getPos(input[i])];
		}
		if (i != input.size())cout << "<invalid input string>" << endl;
		else{
			bool hasNon0 = false;
			do{
				hasNon0 = false;
				for (int i = 0; i < 36; ++i){
					if (ch[i] > 0){
						cout << getCha(i);
						--ch[i];
					}
					if (ch[i] != 0)hasNon0 = true;
				}
			} while (hasNon0);
			cout << endl;
		}
		cin >> input;
	}
	return 0;
}