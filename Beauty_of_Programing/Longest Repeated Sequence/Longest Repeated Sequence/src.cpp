#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>

using namespace::std;

int main(){
	int n;
	scanf("%d", &n);
	vector<int> seq;
	for (int i = 0; i < n; i++){
		int d;
		scanf("%d", &d);
		seq.push_back(d);
	}
	int k = 0, max = 0, M = 0;
	for (; k < n; k++){
		int len = 0;
		int j = k + 1;
		if (j >= n)break;
		for (; j < n; j++) {
			len = 0;
			while (j < n){
				if (seq[j] == seq[k])break;
				j++;
			}
			if (j != n){
				int bound = j, index = k;
				for (; index < bound && j < n && seq[index] == seq[j]; index++, j++)
					len++;
				if (len > max)max = len;
				j--;
			}
		}
		if (max > M)M = max;
	}
	printf("%d", M);
	return 0;
}