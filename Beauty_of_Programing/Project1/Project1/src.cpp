#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <map>
#include <string>
#include <vector>
#include <iostream>
using namespace::std;

int main() {
	int T, N, M;
	map<string, string> sm;
	scanf("%d", &T);
	for (int i = 0; i < T; i++){
		scanf("%d %d", &N, &M);
		for (int j = 0; j < M; j++){
			char w1[21], w2[21];
			scanf("%s %s", w1, w2);
			sm.insert(pair<string, string>(*(new string(w1)), *(new string(w2))));
		}
		char sent[101];
		getchar();
		gets(sent);
		string setStr(sent);
		vector<string> words;
		int start = 0;
		int k = 0;
		for (; k < setStr.length(); k++){
			if (setStr[k] == ' '){
				words.push_back(setStr.substr(start, k - start));
				start = k + 1;
			}
		}
		words.push_back(setStr.substr(start, k - start));
		map<string, string>::iterator it;
		for (int i = 0; i < N - 1; i++){
			for (int j = 0; j < words.size(); j++){
				it = sm.find(words[j]);
				if (it != sm.end())words[j] = it->second;
			}
		}
		string Bob = words[0];
		for (int i = 1; i < words.size(); i++)Bob += (" " + words[i]);
		printf("Case #%d: %s\n", i + 1, Bob.c_str());
		sm.clear();
	}
	return 0;
}