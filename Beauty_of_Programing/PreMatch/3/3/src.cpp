#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
#include <set>
using namespace ::std;
int min_n(set<int> & nl, set<int> & al, int l, int size, vector<vector<int> > & mx, int count, int & max);
int min_m(set<int> & nl, set<int> & al, int l, int size, vector<vector<int> > & mx, int count, int & max);
int find_min_m(vector<int> v);
int find_min_n(vector<vector<int> > mx, int n);
int main(){
	int T, M, N;
	scanf("%d", &T);
	for (int i = 0; i < T; i++){
		int d, count = 0;
		scanf("%d %d", &M, &N);
		vector<vector<int> > mx;
		for (int j = 0; j < M; j++){
			vector<int> line(N, 0);
			for (int k = 0; k < N; k++){
				scanf("%d", &d);
				line[k] = d;
			}
			mx.push_back(line);
		}
		set<int> nl, al;
		int max = 10000, res = 0;
		if (M >= N){
			for (int i = 0; i < M; i++)nl.insert(i);
			res = min_m(nl, al, 0, mx.size(), mx, count, max);
		}
		else{
			for (int i = 0; i < N; i++)nl.insert(i);
			res = min_n(nl, al, 0, mx[0].size(), mx, count, max);
		}
		printf("Case %d: %d\n", i + 1, max);
	}
	return 0;
}
int min_n(set<int> & nl, set<int> & al, int l, int size, vector<vector<int> > & mx, int count, int & max){
	int Min = 10000, index = 0, min_x = 10000;
	if (l == mx.size())return 0;
	for (int j = 0; j < size; j++){
		set<int> ::iterator it = al.find(j);
		if (it != al.end())continue;
		set<int> temp = al, t = nl;
		temp.insert(j);
		t.erase(j);
		if (l == mx.size() - 1){
			count += mx[l][j];
			for (set<int>::iterator it = t.begin(), e = t.end(); it != e; it++)count += (find_min_n(mx, *it));
			if (count < max){
				max = count;
			}
			return 0;
		}
		int res = (min_n(t, temp, l + 1, mx[0].size(), mx, mx[l][j] + count, max));
	}
	return 0;
}

int min_m(set<int> & nl, set<int> & al, int l, int size, vector<vector<int> > & mx, int count, int & max){
	int Min = 10000, index = 0, min_x = 10000;
	for (int j = 0; j < size; j++){
		set<int> ::iterator it = al.find(j);
		if (it != al.end())continue;
		set<int> temp = al, t = nl;
		temp.insert(j);
		t.erase(j);
		if (l == mx[0].size() - 1){
			count += mx[j][l];
			for (set<int>::iterator it = t.begin(), e = t.end(); it != e; it++)count += (find_min_m(mx[*it]));
			if (count < max){
				max = count;
			}
			return 0;
		}
		int res = (min_m(t, temp, l + 1, mx.size(), mx, mx[j][l] + count, max));
	}
	return 0;
}

int find_min_m(vector<int> v){
	int min = v[0];
	for (int i = 0; i < v.size(); i++){
		if (v[i] < min)min = v[i];
	}
	return min;
}

int find_min_n(vector<vector<int> > mx, int n){
	int min = mx[0][n];
	for (int i = 0; i < mx.size(); i++){
		if (mx[i][n] < min)min = mx[i][n];
	}
	return min;
}