#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <cmath>
using namespace::std;

int main(){
	int T;
	long long N;
	scanf("%d", &T);
	for (int i = 0; i < T; i++){
		scanf("%lld", &N);
		long long count = 0;
		for (long long n = 1; n <= N; n++){
			for (long long m = 1; m <= N; m++){
				for (long long l = 1; l <= N; l++){
					if (n == m && n != l || n == l && m != n || m == l && n != m)continue;
					long long nl = N + 1 - n, ml = N + 1 - m, ll = N + 1 - l;
					count += (nl * ml * ll);
				}
			}
		}
		long long res = count % (long long)(pow(10.0, 9.0) + 7);
		printf("Case %d: %lld\n", i + 1, res);
	}
	return 0;
}