#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <stdio.h>

using namespace::std;

int main(){
	int N;
	scanf("%d", &N);
	int i = 0;
	vector<double> to9;
	for (; i < N; i++){
		double o1, o2, res;
		char op;
		scanf("%lf %c %lf", &o1, &op, &o2);
		switch (op){
		case '+':
			res = o1 + o2;
			break;
		case '-':
			res = o1 - o2;
			break;
		case '*':
			res = o1 * o2;
			break;
		case '/':
			res = o1 / o2;
			break;
		}
		to9.push_back((9 - res > 0)?(9 - res):(res - 9));
	}
	double min = to9[0];
	int index = 0;
	for (int i = 0; i < to9.size(); i++)
	if (min > to9[i]){
		min = to9[i];
		index = i;
	}
	printf("%d", index + 1);
	return 0;
}