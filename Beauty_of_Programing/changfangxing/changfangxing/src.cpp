#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

using namespace ::std;

int C(int m){
	return (m * (m - 1) / 2);
}
int main(){
	int T, N, M, K;
	scanf("%d", &T);

	for (int i = 0; i < T; i++){
		scanf("%d %d %d", &N, &M, &K);
		if (N < 2 || M < 2){
			printf("Case #%d: %d\n", i + 1, 0);
			continue;
		}
		int count = 0, k = K, max = 0;
		for (int m = 1; m < M; m++){
			for (int n = 1; n < N; n++){
				count = 0;
				int min = ((m < n) ? m : n), M = ((m > n) ? m : n);
				k = K - (m + 1) * (n + 1);
				if (k < 0 || k > M)continue;
				else {
					count += (C(m + 1) * C(n + 1));
					if (k > 1)count += ((k - 1) * (min + 1) + C(k) - (k - 1));
				}
			}
			if (count > max)max = count;
		}
		printf("Case #%d: %d\n", i + 1, max);
	}
	return 0;
}