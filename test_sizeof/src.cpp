#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace::std;
void foo(char * x) {
	*(++x) = 'a';
}

class Base {
public:
	Base(){cout<<"base\n";};
	class Nest{
	public:
		Nest(){cout<<"Nest\n";};
	};
	Nest n;
};
class Derived:public Base{
public:
	Derived(){cout<<"Derived\n";};
};
int main() {
	Derived d;
	return 0;
}