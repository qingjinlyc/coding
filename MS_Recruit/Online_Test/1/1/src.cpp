#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace ::std;

int compare(const void * a, const void * b);
int main(){
	string str;
	char c[1024];
	int res = scanf("%s", c);
	while (res != 0){
		qsort(c, strlen(c), sizeof(char), compare);
		res = scanf("%s", c);
	}
	return 0;
}

int compare(const void * a, const void * b){
	char x = *((char *)a), y = *((char *)b);
	return x - y;
}
