#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vector>
using namespace::std;

int main(){
	int T, N, M;
	long long K;
	scanf("%d", &T);
	for (int i = 0; i < T; i++){
		scanf("%d %d %lld", &N, &M, &K);
		char * res = new char[N + M + 1];
		if (M == 0 && N == 0){
			res[0] = 0;
		}
		else if (N == 0){
			if (K > 1)res[0] = 0;
			else{
				for (int i = 0; i < M; i++)res[i] = '1';
			}
		}
		else if(M == 0){
			if (K > 1)res[0] = 0;
			else{
				for (int i = 0; i < N; i++)res[i] = '0';
			}
		}
		else {
			res[0] = 1;
			vector<int> pos;
			for (int i = 0; i < M; i++)pos.push_back(N + i);
			int p = 1;
			for (long long j = 0; j < K - 1; j++, p++){
				if (p == 1 && pos[1] == pos[0] + 1){
					pos[0]--;
					if (pos[0] < 0){
						res[0] = 0;
						break;
					}
					for (int i = 1; i < M; i++)pos[i] = N + i;
					p = 0;
					continue;
				}
				if (pos[M - 1] - pos[p] == (M - p)){
					for (int i = p + 1; i < M; i++)pos[i] = N + i;
				}
				pos[p]--;
				if (p == M - 1){
					int l;
					for (l = M - 1; l > 0 && pos[l] - pos[l - 1] <= 1; l--);
					p = l - 1;
				}
			}
			if (res[0] != 0){
				for (int i = 0; i < pos.size(); i++){
					res[pos[i]] = '1';
				}
				for (int i = 0; i < N + M; i++){
					if (res[i] != '1')res[i] = '0';
				}
				res[N + M] = 0;
			}
		}
		if (res[0] != 0)printf("%s\n", res);
		else
			printf("Impossible\n");
	}
	return 0;
}