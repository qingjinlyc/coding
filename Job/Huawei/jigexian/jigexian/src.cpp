#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int compare(const void * n1, const void * n2);
int main(){
	int score[10];
	for (int i = 0; i < 10; i++)scanf("%d", &score[i]);
	qsort(score, 10, sizeof(int), compare);
	if (score[9] >= 60){
		printf("%d", 60);
		return 0;
	}
	printf("%d", score[5] / 10 * 10);
	return 0;
}

int compare(const void * n1, const void * n2){
	int x = *((int *)n1), y = *((int *)n2);
	return x - y < 0;
}