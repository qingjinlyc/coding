#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main(){
	int n;
	scanf("%d", &n);
	if (n == 1){
		printf("%d", 1);
		return 0;
	}
	int d = n / 2;
	while (d * d > n)d--;
	printf("%d", d);
	return 0;
}